@extends('frontend.layouts.app')

@section('title','Buat Iklan')

@section('content')
@stack('before-style')
<style>
    [contenteditable="true"] {
        display: inline-block !important;
        font-weight: 600;
        text-decoration: underline;
    }
    .contain-iklan {
        font-family: 'Nunito', sans-serif;
        font-size: 15px;
        letter-spacing: 0.04em;
        display: inline-block !important;
    }
    .card-footer {
        padding: 7px 20px 7px 20px;
    }
    textarea.form-control {
        border: none;
        color: #3c4b64;
        font-size: large;
        font-weight: 500;
    }
</style>
@stack('after-style')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <nav aria-label="breadcrumb" role="navigation">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="/headline-generator">Headlines Generator</a></li>
                <li class="breadcrumb-item active" aria-current="page">Buat Iklan</li>
                </ol>
            </nav>
            <div class="row">
                <div class="col-12">
                    @php
                    $n      = 0;
                    @endphp
                    @for ($i = 0; $i <= 49; $i++)
                        <form class="card overflow-hidden" method="POST" action="{{ route('frontend.user.headlines.buatiklansimpan') }}">
                            @csrf
                            <div class="display-4 position-absolute" style="color: #626e8047; font-size: 35px">
                                {{ ++$n }}
                            </div>
                            <div class="card-body d-flex align-items-center contain-iklan" style="">
                                {{-- <input hidden value="{{ array($headline) }}"> --}}
                                <input hidden id="i{{ $n }}" value="{{ $headline[$i] }}">
                                <input hidden name="idheadline" value="{{ $i }}">
                                <textarea id="t{{ $n }}" name="isiheadline" class="form-control">{{ $headline[$i] }}</textarea>
                            </div>
                            <div class="card-footer">
                                <div class="float-right">
                                    <button style="background-color: #3AB5B9; border-color: #3AB5B9; color:white" type="button" onclick="reset(i{{ $n }}, t{{ $n }})" class="btn btn-warning btn-pill pl-3 pr-3 btn-sm shadow-sm">
                                        <i class="nav-icon icon-action-undo"></i>
                                        Reset
                                    </button>
                                    <button style="background-color: #0EB6D9; border-color: #0EB6D9; color:white" data-clipboard-target="#t{{ $n }}" type="button" class="btn btn-info btn-pill pl-3 pr-3 btn-sm shadow-sm">
                                        <i class="nav-icon icon-layers"></i>
                                        Salin
                                    </button>
                                    <button type="submit" class="btn btn-primary btn-pill pl-3 pr-3 btn-sm shadow-sm">
                                        <i class="nav-icon icon-note"></i>
                                        Simpan
                                    </button>
                                </div>
                            </div>
                        </form>
                    @endfor

                </div>
            </div>
        </div><!--col-md-10-->
    </div><!--row-->
</div><!--container-->
@stack('before-scripts')
<script type='text/javascript' src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/clipboard@2.0.6/dist/clipboard.min.js"></script>

<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(document).ready(function() {
        function reset(idreset, idtextarea){
            document.getElementById(idtextarea).value = document.getElementById(idreset).value;
        }

        var clipboard = new ClipboardJS('.btn');

        clipboard.on('success', function(e) {
            const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 4000,
            timerProgressBar: true,
            onOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
            })

            Toast.fire({
            icon: 'success',
            title: 'Berhasil Tersalin !',
            footer: e.text

            })
        });

        // clipboard.on('error', function(e) {
        //     console.error('Action:', e.action);
        //     console.error('Trigger:', e.trigger);
        // });

    });



</script>
{{-- @for ($i = 1; $i <= 50; $i++)

$('#crud-submit{{ $i }}').click(function(e){
e.preventDefault();
    $.ajax({
            dataType: 'json',
            method: 'POST',
            url: '{{ route("frontend.user.headlines.simpan") }}',
            data: {
                "_token": "{{ csrf_token() }}",
                "headline": document.getElementById('#h{{ $i }}') .val();
                },
            success: function () {
                console.info('Berhasil', e.action);
            }
        }).done(function(data){
            console.info('Berhasil Selesai', e.action);
        });

    }
);
@endfor --}}
@stack('after-scripts')
@endsection
