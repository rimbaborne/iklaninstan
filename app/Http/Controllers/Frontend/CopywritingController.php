<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Copywriting;
use RealRashid\SweetAlert\Facades\Alert;

class CopywritingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('frontend.user.copywriting.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $copywriting = new Copywriting;

            $copywriting->keterangan_copywritings   = $request->copywriting;
            $copywriting->contoh_copywritings       = $request->contoh;
            $copywriting->status_copywritings       = 'AKTIF';
            $copywriting->kategori_copywritings     = 'PERTAMA'; //masi difikirkan, intinya kategori per event
            $copywriting->jenis_copywritings        = 'UMUM';  //nanti bisa jenis copywriting fashion, makanan, properti, dll

            $copywriting->save();

            toast('Berhasil !', 'success');
        } catch (\Throwable $th) {
            toast('Gagal !', 'error');
        }

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
