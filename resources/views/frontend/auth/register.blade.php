@extends('frontend.layouts.app-a')

@section('title', __('Register'))

@section('content')
    <div class="row justify-content-center mt-5">
        <div class="col-md-5">
            <x-frontend.card>
                {{-- <x-slot name="header">
                    @lang('Register')
                </x-slot> --}}

                <x-slot name="body">
                    <h2 class="text-center">Daftar</h2>
                    <h5 class="text-center pb-4">Iklan Instan</h5>

                    <x-forms.post :action="route('frontend.auth.register')">
                        <div class="col-md-12">
                            <div class="form-group row text-muted font-weight-bold">
                                <label for="name">Nama Lengkap</label>
                                <input type="text" name="name" id="name" class="form-control" value="{{ old('name') }}" maxlength="100" required autofocus autocomplete="name" />
                            </div><!--form-group-->

                            <div class="form-group row text-muted font-weight-bold">
                                <label for="email">Alamat Email</label>
                                <input type="email" name="email" id="email" class="form-control" value="{{ old('email') }}" maxlength="255" required autocomplete="email" />
                            </div><!--form-group-->

                            <div class="form-group row text-muted font-weight-bold">
                                <label for="password">Password</label>
                                <input type="password" name="password" id="password" class="form-control" maxlength="100" required autocomplete="new-password" />
                            </div><!--form-group-->

                            <div class="form-group row text-muted font-weight-bold">
                                <label for="password">Konfirmasi Password</label>
                                <input type="password" name="password_confirmation" id="password_confirmation" class="form-control" maxlength="100" required autocomplete="new-password" />
                            </div><!--form-group-->
                        </div>

                        <div class="form-group row" style="display: none">
                            <div class="col-md-12">
                                <div class="form-check">
                                    <input type="checkbox" name="terms" value="1" id="terms" class="form-check-input" checked>
                                    <label class="form-check-label" for="terms">
                                        @lang('I agree to the') <a href="{{ route('frontend.pages.terms') }}" target="_blank">@lang('Terms & Conditions')</a>
                                    </label>
                                </div>
                            </div>
                        </div><!--form-group-->

                        @if(config('boilerplate.access.captcha.registration'))
                            <div class="row">
                                <div class="col">
                                    @captcha
                                    <input type="hidden" name="captcha_status" value="true" />
                                </div><!--col-->
                            </div><!--row-->
                        @endif

                        <div class="form-group row mb-0">
                            <div class="col-md-12">
                                <button class="btn btn-primary btn-block" type="submit">@lang('Register')</button>
                            </div>
                        </div><!--form-group-->
                        <div class="row" style="padding-top: 10px">
                            <div class="col">
                                <div class="text-left">
                                    <a href="{{ route('frontend.auth.login') }}">
                                        <i class="fas fa-angle-double-left"></i> Login
                                    </a>
                                </div>
                            </div>
                        </div>

                    </x-forms.post>
                </x-slot>
            </x-frontend.card>
        </div><!--col-md-8-->
    </div><!--row-->
    @stack('before-scripts')
    <script type="text/javascript">
        var password = document.getElementById("password")
        , confirm_password = document.getElementById("password_confirmation");

        function validatePassword(){
        if(password.value != confirm_password.value) {
            confirm_password.setCustomValidity("Passwords Berbeda");
        } else {
            confirm_password.setCustomValidity('');
        }
        }

        password.onchange = validatePassword;
        confirm_password.onkeyup = validatePassword;
    </script>
    @stack('after-scripts')

@endsection
