@extends('backend.layouts.app')

@section('title', 'Copywriting')

@section('content')

<div class="fade-in">
    <div class="card">
        <div class="card-body">
            <form onmouseover="verifikasi()" class="form-horizontal col-md-12" action="{{ route('admin.copywriting.generator.data.upload') }}" method="POST" enctype="multipart/form-data" style="padding-top: 20px">
                <div class="form-group row" style="margin-bottom:0px">
                    {{ csrf_field() }}
                    <label class="col-md-1 col-form-label" for="file-input">
                        Pilih File  :
                    </label>
                    <div class="col-md-5">
                        <input type="file" name="file" class="form-control" id="upload" required>

                        {{-- <div class="custom-file">
                            <input type="file" name="file" class="custom-file-input" id="upload" required>
                            <label class="custom-file-label" for="upload">Pilih File Tambah Data Copywriting</label>
                        </div> --}}
                    </div>
                    <div class="col-md-2">
                        <button id="btnupload" type="submit" class="btn btn-primary btn-block" >Upload File</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="card">
        <div class="card-header">
            Data Copywriting
        </div><!--card-header-->
        <div class="card-body">
            @if (request()->metode == 'edit')
                <form class="col-12" action="#">
                    @csrf
                    <input type="hidden" name="metode" value="update">
                    <input type="hidden" name="id" value="{{ $data->id }}">
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Nama Copywriting</label>
                        <div class="col-sm-10">
                            <input name="copywriting" value="{{ $data->keterangan_copywritings }}" class="form-control" type="text" value="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Contoh Copywriting</label>
                        <div class="col-sm-10">
                            <input name="contoh" value="{{ $data->contoh_copywritings }}" class="form-control" type="text">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Jenis Copywriting</label>
                        <div class="col-sm-4">
                            <select id="jenis" name="jenis" class="form-control">
                                <option value="{{ $data->jenis_copywritings }}">{{ $data->jenis_copywritings }}</option>
                                <option value="">-----</option>
                                @foreach ($jenis as $jns)
                                    <option value="{{ $jns->nama }}">{{ $jns->nama }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <a href="{{ route('admin.copywriting') }}" class="btn btn-light">Kembali</a>

                    <button type="submit" class="btn btn-primary">Update</button>
                </form>
            @else
                <form class="col-12" action="#">
                    @csrf
                    <input type="hidden" name="metode" value="tambah">
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Nama Copywriting</label>
                        <div class="col-sm-10">
                            <input name="copywriting" class="form-control" type="text">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Contoh Copywriting</label>
                        <div class="col-sm-10">
                            <input name="contoh" class="form-control" type="text">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Jenis Copywriting</label>
                        <div class="col-sm-4">
                            <select id="jenis" name="jenis" class="form-control">
                                @foreach ($jenis as $jns)
                                    <option value="{{ $jns->nama }}">{{ $jns->nama }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </form>
        </div><!--card-body-->
    </div><!--card-->
    <div class="card">
        <div class="card-body">
            <div class="col-12">
                <table class="table table-sm">
                    <thead>
                        <th class="text-center">No</th>
                        <th>Nama Copywriting</th>
                        <th>Jenis</th>
                        <th width="150"></th>
                    </thead>
                    <tbody>
                        @foreach ($copywritings as $key => $headline)
                        <tr>
                            <td class="text-center">{{ $key + $copywritings->firstItem() }}</td>
                            <td>
                                {{ $headline->keterangan_copywritings }}
                                <div class="text-muted">
                                    Contoh: {{ $headline->contoh_copywritings }}
                                </div>
                            </td>
                            <td>{{ $headline->jenis_copywritings }}</td>
                            <td>
                                <div class="btn-group" role="group" aria-label="Default button group">
                                    <a href="{{ route('admin.copywriting') }}/?metode=edit&id={{ $headline->id }}" class="btn btn-sm btn-success" type="button">Edit</a>
                                    <form action="#" onsubmit="return confirm('Apakah Yakin Dihapus ?');">
                                        @csrf
                                        <input type="hidden" name="metode" value="hapus">
                                        <input name="id" value="{{ $headline->id }}" hidden>
                                        <button class="btn btn-sm btn-danger" >Hapus</button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                        @php
                        $first = $copywritings->firstItem();
                        $end = $key + $copywritings->firstItem();
                        @endphp
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="col-12">
                <div class="row" style="padding-top:10px">
                    <div class="col-7">
                        <div class="float-left" style="padding-left:10px">
                            {!! $first !!} - {!! $end !!} Dari {!! $copywritings->total() !!} Data
                        </div>
                    </div>
                    <!--col-->

                    <div class="col-5">
                        <div class="float-right" style="padding-right:10px">
                            {!! $copywritings->appends(request()->query())->links() !!}
                        </div>
                    </div>
                    <!--col-->
                </div>
            </div>
            @endif

        </div><!--card-body-->
    </div><!--card-->
</div>
<script>
    function verifikasi() {
	    var btn = document.getElementById("btnupload");
        if (document.getElementById("upload").value==="") {
            btn.disabled = true;
        } else {
            btn.disabled = false;
        }
    }
</script>
@endsection
