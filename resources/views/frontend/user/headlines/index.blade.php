@extends('frontend.layouts.app')

@section('title','Headlines Generator')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        {{-- @if (!null == request('jenis')) --}}
            <div class="col-md-10">
                <nav aria-label="breadcrumb" role="navigation">
                    <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Headlines Generator</li>
                    </ol>
                </nav>
                {{-- <div class="form-group row">
                    <label class="col-md-2 col-form-label text-center">Jenis Headline</label>
                    <form class="col-md-4">
                        <select id="jenis" name="jenis" onchange='if(this.value != 0) { this.form.submit(); }' class="form-control">
                            @foreach ($jenis as $jns)
                                <option value="{{ $jns->nama }}">{{ $jns->nama }}</option>
                            @endforeach
                        </select>
                    </form>
                </div> --}}
                <div class="row">
                    <div class="col-12">
                        <div class="card overflow-hidden">
                            <div class="card-header">
                                <div class="c-callout c-callout-primary">
                                    <div class="display-4" style="font-size: 30px">
                                        Headlines Generator
                                    </div>
                                    <div class="text-muted">
                                        Form Input
                                    </div>
                                </div>
                            </div>
                            <div class="card-body d-flex align-items-center">
                                <form class="col-12" action="{{ route('frontend.user.headlines.simpan') }}" method="post">
                                    @csrf
                                    @foreach ($headlines as $headline)
                                        <div class="form-group">
                                            <label class="label-headlines">{{ $headline->keterangan_headlines }}</label>
                                            <input name="headline[]" class="form-control" type="text" placeholder="Masukkan Jawaban Anda" value="{{ $headline->contoh_headlines }}" >
                                            <input name="idheadline[]" value="{{ $headline->id }}" type="text" hidden>
                                            <div class="text-muted">
                                                Contoh : {{ $headline->contoh_headlines }}
                                            </div>
                                        </div>
                                        <hr>
                                    @endforeach

                                    <div class="form-group">
                                        {{-- <button class="btn btn-info" type="submit">Simpan Data</button> --}}
                                        <button class="btn btn-primary" type="submit" formaction="{{ route('frontend.user.headlines.buatiklan.temp') }}">Buat Iklan</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--col-md-10-->
            @stack('before-scripts')
                <script type="text/javascript">
                    $(document).ready(function(){
                            $("#jenis").val("{!! request('jenis') !!}");
                    });
                </script>
            @stack('after-scripts')
        {{-- @else
            <div class="col-md-10 pt-4 mt-4">
                <div class="form-group row justify-content-center">
                    <h2 class="col-md-10 text-center display">Pilih jenis Bisnis Anda ?</h2>
                    <form class="col-md-10">
                        <select name="jenis" onchange='if(this.value != 0) { this.form.submit(); }' class="form-control form-control-lg" style="border: 1px solid #0F75BC; border-radius: 25px; font-weight: 800;">
                            <option value="">Pilih...</option>
                            @foreach ($jenis as $jns)
                                <option value="{{ $jns->nama }}">{{ $jns->nama }}</option>
                            @endforeach
                        </select>
                    </form>
                </div>
            </div>
        @endif --}}
    </div><!--row-->
</div><!--container-->
@endsection
