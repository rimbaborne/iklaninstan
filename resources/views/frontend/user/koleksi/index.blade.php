@extends('frontend.layouts.app')

@section('title', 'Koleksi')

@section('content')
@stack('before-style')
<style>
textarea.form-control {
    border: none;
    color: #3c4b64;
    font-size: medium;
    font-weight: 500;
}
</style>
@stack('after-style')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <nav aria-label="breadcrumb" role="navigation">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Koleksi</li>
                </ol>
            </nav>
            <div class="card overflow-hidden">
                <div class="card-body">
                    <form class="row" action="{{ route('frontend.user.koleksi') }}" method="get">
                        <div class="col-2">
                            <select  onchange='if(this.value != 0) { this.form.submit(); }' name="paged" style="border: 1px solid #d8dbe0; border-radius: 5px; padding: 5px;">
                                <option>{{ $paged }}</option>
                                <option>---</option>
                                <option>5</option>
                                <option>10</option>
                                <option>25</option>
                                <option>50</option>
                                <option>100</option>
                                <option>150</option>
                            </select>
                        </div>
                        <div class="col-4">
                            <input name="cari" class="form-control" placeholder="Cari">
                        </div>

                        <div class="col-1 text-center">
                        </div>
                        <div class="col-5 text-right">
                            <a href="{{ route('frontend.user.headlines.buatiklan') }}">
                                <button data-toggle="tooltip" data-placement="top" title="" data-original-title="Pembaruan Headline Iklan Terakhir" type="button" class="btn btn-warning btn-pill pl-3 pr-3">
                                    <i class="nav-icon icon-action-undo"></i>
                                    Headline Iklan
                                </button>
                            </a>
                        </div>

                        {{-- <div class="col-md-1 text-center" style="padding-left: 0px">
                            <button class="btn btn-primary btn-block"><i class="fa fa-search"></i> Cari</button>
                        </div> --}}
                    </form>
                </div>
                <div class="card-body d-flex align-items-center" style=" overflow-x: scroll;">
                    <table class="table table-hover table-outline mb-0" style="min-width: 600px;"">
                        <thead class="thead-light">
                            <tr>
                                <th width="50" class="text-center">
                                    <i class="nav-icon icon-chart"></i>
                                </th>
                                <th class="text-center">Koleksi Headline</th>
                                <th width="200"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                            $first  = 0;
                            $end    = 0;
                            $number = 1;
                            @endphp
                            @foreach ($listheadlines as $key => $list)
                            <tr>
                                <td class="text-center">
                                    <div class="c-avatar">
                                        <strong>{{ $key + $listheadlines->firstItem() }}</strong>
                                    </div>
                                </td>
                                <td>
                                    <textarea class="form-control" id="h{{ $key + $listheadlines->firstItem() }}">Headline {{ $list->isi_user_koleksi }}</textarea>
                                </td>
                                <td class="" id="">
                                    <button type="button" data-clipboard-target="#h{{ $key + $listheadlines->firstItem() }}" class="btn btn-info btn-sm">
                                        <i class="fas fa-copy"></i> Salin
                                    </button>

                                    {{-- <a href="/koleksi/edit?id={{ $list->uuid }}" class="btn btn-primary btn-sm">
                                        <i class="fas fa-pencil-alt"></i> Edit
                                    </a> --}}

                                    <form method="POST" action="{{ route('frontend.user.koleksi.hapus') }}" name="delete-item" class="d-inline">
                                        @csrf
                                        <input type="hidden" name="uuid" value="{{ $list->uuid }}">
                                        <button type="submit" class="btn btn-danger btn-sm">
                                            <i class="fas fa-trash"></i> Hapus
                                        </button>
                                    </form>
                                    <div class="text-muted pull-right">
                                        {{ $list->updated_at->diffForHumans() }}
                                    <div class="small text-muted">{{ $list->created_at }} </div>
                                    </div>
                                </td>
                            </tr>
                            @php
                            $first  = $listheadlines->firstItem();
                            $end    = $key + $listheadlines->firstItem();
                            @endphp
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-7">
                            <div class="float-left">
                                {!! $first !!} - {!! $end !!} Dari {!! $listheadlines->total() !!} Data
                            </div>
                        </div>

                        <div class="col-5">
                            <div class="float-right">
                                {!! $listheadlines->appends(request()->query())->links() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!--col-md-10-->
    </div><!--row-->
</div><!--container-->
@stack('before-scripts')
<script type='text/javascript' src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/clipboard@2.0.6/dist/clipboard.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {

        var clipboard = new ClipboardJS('.btn');

        clipboard.on('success', function(e) {
            const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 4000,
            timerProgressBar: true,
            onOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
            })

            Toast.fire({
            icon: 'success',
            title: 'Berhasil Tersalin !',
            footer: e.text

            })
        });
    });
</script>
@stack('after-scripts')
@endsection
