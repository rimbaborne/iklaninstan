<?php

namespace App\Http\Controllers\Backend;

use App\Models\JenisBisnis;
use App\Models\Copywriting;
use App\Models\CopywritingGenerator;
use App\Http\Controllers\Controller;
use App\Imports\CopywritingsImport;
use Maatwebsite\Excel\Facades\Excel;

/**
 * Class CopywritingController.
 */
class CopywritingController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $jenis        = JenisBisnis::get();
        $copywritings = Copywriting::orderByDesc('id')->paginate(10);
        $copywriting  = new Copywriting();


        if (request()->metode == 'tambah') {

            $copywriting->keterangan_copywritings = request()->copywriting;
            $copywriting->contoh_copywritings = request()->contoh;
            $copywriting->jenis_copywritings = request()->jenis;

            $copywriting->save();

            toast('Berhasil Ditambah !','success');
            return redirect()->back();
        }

        if (request()->metode == 'hapus') {

            $data = $copywriting->find(request()->id);
            $data->delete();

            toast('Berhasil Dihapus !','warning');
            return redirect()->back();
        }

        if (request()->metode == 'edit') {

            $data = $copywriting->find(request()->id);
        } else {
            $data = null;
        }

        if (request()->metode == 'update') {
            $data = $copywriting->find(request()->id);

            $data->keterangan_copywritings = request()->copywriting;
            $data->contoh_copywritings = request()->contoh;
            $data->jenis_copywritings = request()->jenis;

            $data->save();

            toast('Berhasil Diupdate !','info');
            return redirect()->back();
        }

        return view('backend.copywriting.index', compact('jenis', 'copywritings', 'data'));
    }


    public function generator()
    {
        $jenis                 = JenisBisnis::get();
        $copywritinggenerators = CopywritingGenerator::orderByDesc('id')->paginate(10);
        $copywritinggenerator  = new CopywritingGenerator();
        $copywritings          = Copywriting::where('jenis_copywritings', request()->jenis)->get() ?? null;

        $datacp[] = [
            'type'  => 'choiceitem',
            'text'  => '----',
            'value' => '----',
        ];

        $dataawal[] = null;
        $dataakhir[] = null;

        if (request()->metode == 'tambah') {

            $copywritinggenerator->nama_copywriting_generators = request()->nama;
            $copywritinggenerator->data_copywriting_generators = request()->data;
            $copywritinggenerator->jenis_copywriting_generators = request()->jenis;

            $copywritinggenerator->save();

            toast('Berhasil Ditambah !','success');
            return redirect()->route('admin.copywriting.generator');
        }

        if (request()->metode == 'hapus') {

            $data = $copywritinggenerator->find(request()->id);
            $data->delete();

            toast('Berhasil Dihapus !','warning');
            return redirect()->back();
        }

        if (request()->metode == 'edit') {
            $data = $copywritinggenerator->find(request()->id);
            $copywritings          = Copywriting::where('jenis_copywritings', $data->jenis_copywriting_generators)->get() ?? null;

            foreach ($copywritings as $cp){
                $dataawal[]  = ' <strong>{'.$cp->keterangan_copywritings.'}</strong> ';
                $dataakhir[] = ' '.$cp->contoh_copywritings.' ';
            }
        } else {
            $data = null;
        }

        if (request()->metode == 'update') {
            $data = $copywritinggenerator->find(request()->id);

            $data->nama_copywriting_generators = request()->nama;
            $data->data_copywriting_generators = request()->data;
            $data->jenis_copywriting_generators = request()->jenis;

            $data->save();

            toast('Berhasil Diupdate !','info');
            return redirect()->back();
        }

        foreach ($copywritings as $cp){
            $datacp[]  = [
                'type'  => 'choiceitem',
                'text'  => $cp->keterangan_copywritings,
                'value' => ' <strong>{'.$cp->keterangan_copywritings.'}</strong> ',
            ];
        }



        return view('backend.copywriting.generator', compact('jenis', 'copywritinggenerators', 'data', 'copywritings', 'datacp', 'dataawal', 'dataakhir'));
    }

    public function generatordata()
    {
        $copywritings  = Copywriting::where('jenis_copywritings', request()->jenis)->get() ?? null;

        $data = [];

        foreach ($copywritings as $cp){

            $data[] = [
                'type' => 'choiceitem',
                'text' => $cp->keterangan_copywritings,
                'value'=> '{'.str_replace(' ', '_',$cp->keterangan_copywritings).'}'
            ];
        }
        return response()->json($data);
    }

    public function generatordataupload()
    {
        if (request()->hasFile('file')) {

            // validasi
            // $this->validate(request()->get('file'), [
            //     'file' => 'required|mimes:csv,xls,xlsx'
            // ]);

            $dataimport = new CopywritingsImport;

            Excel::import($dataimport, request()->file('file'));

            $banyakdata = $dataimport->getRowCount();

            // alihkan halaman kembali
            return redirect()->back()
            ->withFlashSuccess('Upload File Excel Jadwal Berhasil. Total '.$banyakdata.' Data');
        } else {
            return redirect()->back()->withFlashSuccess('Upload Gagal');
        }
    }


}
