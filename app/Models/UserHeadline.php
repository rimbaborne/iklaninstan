<?php

namespace App\Models;

use Ramsey\Uuid\Exception\UnsupportedOperationException;
use Ramsey\Uuid\Uuid;
use Illuminate\Database\Eloquent\Model;

class UserHeadline extends Model
{
    protected $fillable = [
        'uuid',
        'id_headlines',
        'id_user',
        'id_list_headlines',
        'jawaban_user_headlines',
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            try {
                $model->uuid = Uuid::uuid4()->toString();
            } catch (UnsupportedOperationException $e) {
                abort(500, $e->getMessage());
            }
        });
    }
}
