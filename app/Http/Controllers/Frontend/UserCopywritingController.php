<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Copywriting;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Carbon;
use App\Models\UserKoleksi;
use RealRashid\SweetAlert\Facades\Alert;
use App\Models\JenisBisnis;



class UserCopywritingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $copywritings = Copywriting::where('status_copywritings', 'AKTIF')
                                    ->where('jenis_copywritings', request()->jenis ?? null)
                                    ->get();

        if (auth()->user()->active == false) {
            return redirect()->route('frontend.user.dashboard');
        }

        $jenis = JenisBisnis::orderBy('nama')->get();

        return view('frontend.user.copywriting.index', compact('copywritings', 'jenis'));
    }

    //disimpan temporary dulu
    public function buatcopywritingtemp(Request $request)
    {
        $tanggalan        = Carbon::now();

        // ngambil data request
        $copywritings = $request->copywriting ?? null;
        $n            = 0;
        $data         = array();

        foreach ($copywritings as $h) {
            $data[] = [
                "id_copywritings" => $request->idcopywriting[$n++],
                "jawaban_user_copywritings" => $h,
            ];
        }

        // data template copywriting dijadikan array
        $copywriting = array(
            /////// MODEL 1
            'Untuk ' . $data[0]['jawaban_user_copywritings'] . ' yang punya masalah ' . $data[1]['jawaban_user_copywritings'] . '
kita semua tau masalah itu bikin kita jadi ' . $data[4]['jawaban_user_copywritings'] . '

tapi kalau dibiarkan, nanti ujungnya bisa-bisa ' . $data[5]['jawaban_user_copywritings'] . '

karenanya Saya punya solusi untuk ' . $data[0]['jawaban_user_copywritings'] . ', namanya ' . $data[11]['jawaban_user_copywritings'] . '

ini adalah ' . $data[12]['jawaban_user_copywritings'] . ' yang ' . $data[13]['jawaban_user_copywritings'] . '

' . $data[19]['jawaban_user_copywritings'] . ' maka ' . $data[7]['jawaban_user_copywritings'] . '

' . $data[11]['jawaban_user_copywritings'] . ' ini sudah terbukti ampuh untuk membantu ' . $data[6]['jawaban_user_copywritings'] . '

contohnya cerita dari konsumen Saya namanya ' . $data[25]['jawaban_user_copywritings'] . '
katanya ' . $data[24]['jawaban_user_copywritings'] . '

ada juga yang namanya ' . $data[27]['jawaban_user_copywritings'] . '
ceplosannya beberapa waktu lalu ' . $data[26]['jawaban_user_copywritings'] . '

Syukurlah, Saya jadi ikutan senang

Nah buat yang serius mau ' . $data[7]['jawaban_user_copywritings'] . ' tanpa ' . $data[10]['jawaban_user_copywritings'] . '
maka ' . $data[11]['jawaban_user_copywritings'] . ' ini solusi ampuhnya

cuma ' . $data[23]['jawaban_user_copywritings'] . ' tapi masalah ' . $data[0]['jawaban_user_copywritings'] . ' bisa hilang

kebetulan untuk yang ' . $data[30]['jawaban_user_copywritings'] . ' ' . $data[32]['jawaban_user_copywritings'] . '
akan dapat ' . $data[31]['jawaban_user_copywritings'] . '

Untuk info lengkapnya, silahkan langsung ' . $data[33]['jawaban_user_copywritings'] . ' ya

Semoga informasi barusan bermanfaat',

            /////// MODEL 2

            'mungkin ada yang ingin ' . $data[6]['jawaban_user_copywritings'] . '
tapi punya kendala ' . $data[10]['jawaban_user_copywritings'] . '

untungnya ada ' . $data[11]['jawaban_user_copywritings'] . '

ini akan membantu karena ' . $data[18]['jawaban_user_copywritings'] . '
' . $data[19]['jawaban_user_copywritings'] . ' sehingga ' . $data[7]['jawaban_user_copywritings'] . '

untuk bisa memiliki ' . $data[11]['jawaban_user_copywritings'] . ' cuma ' . $data[23]['jawaban_user_copywritings'] . '

yang mau tau lebih banyak, boleh langsung ' . $data[33]['jawaban_user_copywritings'] . '

karena kebetulan sekarang ada ' . $data[31]['jawaban_user_copywritings'],

            /////// MODEL 3

            'Jadi ceritanya, konsumen Saya yang namanya ' . $data[25]['jawaban_user_copywritings'] . '
pernah nyeplos kurang lebih begini ' . $data[24]['jawaban_user_copywritings'] . '
setelah pakai ' . $data[11]['jawaban_user_copywritings'] . '

dan itu bukan hanya 1 atau 2 kali ada konsumen yang nyeplos seperti itu

jujur aja, Saya bersykur bisa membantu orang lain ' . $data[6]['jawaban_user_copywritings'] . ' lewat produk ini ada kesenangan tersendiri gitu, hehe

nah ngomong-ngomong sudah tau belum kenapa koq ' . $data[11]['jawaban_user_copywritings'] . ' ini bisa ampuh banget?

jawabannya mungkin karena produk ini ' . $data[13]['jawaban_user_copywritings'] . '
selain itu ' . $data[14]['jawaban_user_copywritings'] . '
dan ' . $data[15]['jawaban_user_copywritings'] . '
jadinya bisa menghilangkan ' . $data[1]['jawaban_user_copywritings'] . ' dalam ' . $data[17]['jawaban_user_copywritings'] . '

buat yang punya keinginan serupa coba deh pakai ' . $data[11]['jawaban_user_copywritings'] . '
banyak merekomendasikannya',

            /////// MODEL 4

            'Survey ya,

kalau ada ' . $data[12]['jawaban_user_copywritings'] . '
yang bisa menghilangkan ' . $data[1]['jawaban_user_copywritings'] . '
dalam ' . $data[17]['jawaban_user_copywritings'] . '
tanpa ' . $data[10]['jawaban_user_copywritings'] . '

kelebihannya ' . $data[13]['jawaban_user_copywritings'] . '
selain itu juga ' . $data[14]['jawaban_user_copywritings'] . ' sekaligus ' . $data[15]['jawaban_user_copywritings'] . '

berapa banyak nih yang mau? ',

            /////// MODEL 5

            $data[31]['jawaban_user_copywritings'] . '
' . $data[32]['jawaban_user_copywritings'] . '

khusus untuk ' . $data[30]['jawaban_user_copywritings'] . ' ' . $data[12]['jawaban_user_copywritings'] . ' ' . $data[11]['jawaban_user_copywritings'] . ' ya

kelebihan ' . $data[12]['jawaban_user_copywritings'] . '
ini adalah terbuat dari bahan organic ' . $data[14]['jawaban_user_copywritings'] . '
dan ' . $data[15]['jawaban_user_copywritings'] . '

sehingga cocok untuk yang punya masalah ' . $data[1]['jawaban_user_copywritings'] . ' dan masalah ' . $data[2]['jawaban_user_copywritings'] . '

nah semua itu bisa didapat dengan penawaran spesial karena sekarang ada promo khusus

kalau hari biasa, maka ' . $data[31]['jawaban_user_copywritings'] . ' hilang ya
sekali lagi ini hanya sampai ' . $data[32]['jawaban_user_copywritings'] . '

jadi yang mau untung banyak, boleh langsung ' . $data[33]['jawaban_user_copywritings'] . ' sebelum promonya selesai, hehe ',

            /////// MODEL 6

            'bagaimana ' . $data[6]['jawaban_user_copywritings'] . ' tanpa ' . $data[10]['jawaban_user_copywritings'] . '

mungkinkah Anda pernah atau sedang merasakan hal-hal dibawah ini?
' . $data[1]['jawaban_user_copywritings'] . '?
' . $data[2]['jawaban_user_copywritings'] . '? atau
' . $data[3]['jawaban_user_copywritings'] . '?

terkadang, untuk ' . $data[6]['jawaban_user_copywritings'] . ' memang butuh ' . $data[10]['jawaban_user_copywritings'] . '

masalahnya, gak semua orang mampu melakukan itu

akibatnya, banyak yang ' . $data[4]['jawaban_user_copywritings'] . '
padahal, semua masalah itu solusinya adalah ' . $data[11]['jawaban_user_copywritings'] . '

' . $data[11]['jawaban_user_copywritings'] . ' adalah ' . $data[12]['jawaban_user_copywritings'] . ' yang
' . $data[13]['jawaban_user_copywritings'] . ' ' . $data[14]['jawaban_user_copywritings'] . ' dan ' . $data[15]['jawaban_user_copywritings'] . '

sekarang ' . $data[18]['jawaban_user_copywritings'] . ' . ' . $data[16]['jawaban_user_copywritings'] . '

' . $data[0]['jawaban_user_copywritings'] . ' sudah bisa merasakan
' . $data[7]['jawaban_user_copywritings'] . ' , ' . $data[8]['jawaban_user_copywritings'] . ' dan ' . $data[9]['jawaban_user_copywritings'] . '

gak perlu khawatir ' . $data[20]['jawaban_user_copywritings'] . ' karena ' . $data[21]['jawaban_user_copywritings'] . '

bayangkan aja, cuma ' . $data[23]['jawaban_user_copywritings'] . '
dan ' . $data[0]['jawaban_user_copywritings'] . ' dapat solusi dari masalahnya

apalagi, sekarang ' . $data[31]['jawaban_user_copywritings'] . ' jadinya, tambah untung deh

tapi semua itu hanya bisa didapat ' . $data[32]['jawaban_user_copywritings'] . '
setelah itu, maka kesempatan ini akan hilang

karenanya, agar langsung merasakan manfaat dari ' . $data[12]['jawaban_user_copywritings'] . ' ini
' . $data[0]['jawaban_user_copywritings'] . ' bisa langsung ' . $data[33]['jawaban_user_copywritings'] . ' sekarang ya

inilah solusi ampuh untuk yang ingin ' . $data[6]['jawaban_user_copywritings'],

        );

        // disimpan jadi json. nanti dikeluarin biar tetap baca array
        $copywriting = json_encode($copywriting);

        // cek dulu datanya tempnya. kalo sudah ada tinggal update, 1 user -> 1 temp
        $temp_copywriting = DB::table('temp_user_copywritings')->where('id_user', auth()->user()->id)->first();

        if (isset($temp_copywriting)) {
            DB::table('temp_user_copywritings')->where('id_user', auth()->user()->id)->update([
                "copywriting"     => $copywriting,
                "updated_at"      => $tanggalan
            ]);
        } else {
            DB::table('temp_user_copywritings')->insert(
                [
                    "uuid"        => Uuid::uuid4()->toString(),
                    "id_user"     => auth()->user()->id,
                    "copywriting" => $copywriting,
                    "created_at"  => $tanggalan,
                    "updated_at"  => $tanggalan
                ]
            );
        }
        return redirect()->route('frontend.user.copywritings.buatcopywriting');
    }

    // ngambil data dari temporary
    public function buatcopywriting(Request $request)
    {
        // ngambil data dari temp
        $copywriting_ =  DB::table('temp_user_copywritings')->where('id_user', auth()->user()->id)->first();
        if (!isset($copywriting_)) {
            return redirect()->route('frontend.user.copywritings');
        }
        $copywriting  = json_decode($copywriting_->copywriting);

        return view('frontend.user.copywriting.buat', compact('copywriting'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
