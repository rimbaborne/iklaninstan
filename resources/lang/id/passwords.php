<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Password Anda berhasil diperbaruhi!',
    'sent' => 'Kami telah mengirimkan email untuk mengganti password anda',
    'throttled' => 'Mohon tunggu beberapa saat.',
    'token' => 'Sesi untuk mengganti password anda sudah tidak valid. Silahkan ulangi proses anda. Terima kasih.',
    'user' => "Kami tidak dapat menemukan email yang anda masukkan !",

];
