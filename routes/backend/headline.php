<?php

use App\Http\Controllers\Backend\HeadlineController;
use Tabuna\Breadcrumbs\Trail;

Route::get('headline', [HeadlineController::class, 'index'])
    ->name('headline')
    ->breadcrumbs(function (Trail $trail) {
        $trail->push('Headline', route('admin.headline'));
    });

Route::post('headline/upload', [HeadlineController::class, 'dataupload'])
    ->name('headline.upload');
