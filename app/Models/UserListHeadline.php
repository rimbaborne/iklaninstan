<?php

namespace App\Models;

use Ramsey\Uuid\Exception\UnsupportedOperationException;
use Ramsey\Uuid\Uuid;
use Illuminate\Database\Eloquent\Model;

class UserListHeadline extends Model
{
    protected $fillable = [
        'uuid',
        'id_user',
        'nama_user_list_headlines',
        'status_user_list_headlines',
        'jenis_user_list_headlines',
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            try {
                $model->uuid = Uuid::uuid4()->toString();
            } catch (UnsupportedOperationException $e) {
                abort(500, $e->getMessage());
            }
        });
    }
}
