@extends('frontend.layouts.app')

@section('title','Buat Iklan')

@section('content')
@stack('before-style')
<style>
    [contenteditable="true"] {
        display: inline-block !important;
        font-weight: 600;
        text-decoration: underline;
    }
    .contain-iklan {
        font-family: 'Nunito', sans-serif;
        font-size: 15px;
        letter-spacing: 0.04em;
        display: inline-block !important;
    }
    .card-footer {
        padding: 7px 20px 7px 20px;
    }
    textarea.form-control {
        border: none;
        color: #3c4b64;
        font-size:medium;
        font-weight: 500;
        line-height: 19px;
    }
</style>

<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/flickity/1.0.0/flickity.css">
<style>
    .flickity-prev-next-button {
        width: 35px;
        height: 35px;
        background: hsla(0, 0%, 84%, 0.85);
    }
    .flickity-page-dots .dot {
        width: 12px;
        height: 12px;
    }
    .flickity-page-dots {
        bottom: 1145px;
    }
</style>
@stack('after-style')
@stack('after-scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/flickity/1.0.0/flickity.pkgd.js"></script>
    {{-- <script src="https://unpkg.com/flickity-hash@1/hash.js"></script> --}}
@stack('before-scripts')
<div class="container" >
    <div class="row justify-content-center">
        <div class="col-md-10">
            <nav aria-label="breadcrumb" role="navigation">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="/copywriting-generator">Copywritings Generator</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Buat Iklan</li>
                </ol>
            </nav>
            {{-- <div class="text-center">
                MODEL
            </div>
            <nav aria-label="Page navigation example">
                <ul class="pagination justify-content-center">
                    <li class="page-item"><a class="page-link" href="#" aria-label="Previous"><span aria-hidden="true">«</span><span class="sr-only">Previous</span></a></li>
                    @for ($i = 1; $i <= 6; $i++)
                    <li class="page-item"><a class="page-link" href="#p{{ $i }}">{{ $i }}</a></li>
                    @endfor
                    <li class="page-item"><a class="page-link" href="#" aria-label="Next"><span aria-hidden="true">»</span><span class="sr-only">Next</span></a></li>
                </ul>
            </nav> --}}
            <div class="row">
                <div class="col-12" style="margin: 50px 0 150px 0">
                    <div class="js-flickity" data-flickity-options='{ "wrapAround": false, "draggable": false }'>
                        @php
                        $n      = 0;
                        @endphp
                        @for ($i = 0; $i <= 5; $i++)
                        <div class="col-12" id="p{{ ++$n }}">
                            <form class="card overflow-hidden" method="POST" action="#">
                                @csrf
                                <div class="card-header">
                                    <div class="float-left">
                                        <div class="display-4" style="color: #626e80; font-size: 30px; font-weight: 500">
                                            Model {{ $n }}
                                        </div>
                                    </div>
                                    {{-- <div class="float-right">
                                        <button data-clipboard-target="#t{{ $n }}" type="button" class="btn btn-info btn-pill pl-3 pr-3 btn-sm">
                                            <i class="nav-icon icon-layers"></i>
                                            Salin
                                        </button>
                                        <button type="submit" class="btn btn-primary btn-pill pl-3 pr-3 btn-sm">
                                            <i class="nav-icon icon-note"></i>
                                            Simpan
                                        </button>
                                    </div> --}}
                                </div>
                                <div class="card-body d-flex align-items-center contain-iklan" style="">
                                    {{-- <input hidden value="{{ array($copywriting) }}"> --}}
                                    <input hidden id="i{{ $n }}" value="{{ $copywriting[$i] }}">
                                    <input hidden name="idcopywriting" value="{{ $n }}">
                                    <textarea id="t{{ $n }}" rows="50" name="isicopywriting" class="form-control">{{ $copywriting[$i] }}</textarea>
                                </div>
                                <div class="card-footer">
                                    <div class="float-right">
                                        <button style="background-color: #3AB5B9; border-color: #3AB5B9; color:white" type="button" onclick="reset(i{{ $n }}, t{{ $n }})" class="btn btn-warning btn-pill pl-3 pr-3 btn-sm shadow-sm">
                                            <i class="nav-icon icon-action-undo"></i>
                                            Reset
                                        </button>
                                        <button style="background-color: #0EB6D9; border-color: #0EB6D9; color:white" data-clipboard-target="#t{{ $n }}" type="button" class="btn btn-info btn-pill pl-3 pr-3 btn-sm shadow-sm">
                                            <i class="nav-icon icon-layers"></i>
                                            Salin
                                        </button>
                                        {{-- <button type="button" class="btn btn-primary btn-pill pl-3 pr-3 btn-sm">
                                            <i class="nav-icon icon-note"></i>
                                            Simpan
                                        </button> --}}
                                    </div>
                                </div>
                            </form>
                        </div>
                        @endfor
                    </div>
                </div>
            </div>
        </div><!--col-md-10-->
    </div><!--row-->
</div><!--container-->
@stack('before-scripts')
<script type='text/javascript' src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/clipboard@2.0.6/dist/clipboard.min.js"></script>
{{-- <script src="https://cdn.tiny.cloud/1/5g7yssamd0271mz2hhe998gnkav4u9v2a52inq9hul2j7qa4/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script> --}}
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(document).ready(function() {
        function reset(idreset, idtextarea){
            document.getElementById(idtextarea).value = document.getElementById(idreset).value;
        }

        var clipboard = new ClipboardJS('.btn');

        clipboard.on('success', function(e) {
            const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 4000,
                timerProgressBar: true,
                onOpen: (toast) => {
                    toast.addEventListener('mouseenter', Swal.stopTimer)
                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                }
            })

            Toast.fire({
                icon: 'success',
                title: 'Berhasil Tersalin !',

            })
        });

        // clipboard.on('error', function(e) {
            //     console.error('Action:', e.action);
            //     console.error('Trigger:', e.trigger);
            // });

        });
    </script>
    {{-- @for ($i = 1; $i <= 50; $i++)

        $('#crud-submit{{ $i }}').click(function(e){
            e.preventDefault();
            $.ajax({
                dataType: 'json',
                method: 'POST',
                url: '{{ route("frontend.user.copywritings.simpan") }}',
                data: {
                    "_token": "{{ csrf_token() }}",
                    "copywriting": document.getElementById('#h{{ $i }}') .val();
                },
                success: function () {
                    console.info('Berhasil', e.action);
                }
            }).done(function(data){
                console.info('Berhasil Selesai', e.action);
            });

        }
        );
        @endfor --}}
        @stack('after-scripts')
        @endsection
