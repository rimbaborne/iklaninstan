<?php

namespace App\Http\Controllers\Backend;

use App\Models\JenisBisnis;
use App\Models\Headline;
use App\Http\Controllers\Controller;
use App\Imports\HeadlinesImport;
use Maatwebsite\Excel\Facades\Excel;

/**
 * Class HeadlineController.
 */
class HeadlineController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $jenis     = JenisBisnis::get();
        $headlines = Headline::orderByDesc('id')->paginate(10);
        $headline  = new Headline();


        if (request()->metode == 'tambah') {

            $headline->keterangan_headlines = request()->headline;
            $headline->contoh_headlines = request()->contoh;
            $headline->jenis_headlines = request()->jenis;

            $headline->save();

            toast('Berhasil Ditambah !','success');
            return redirect()->back();
        }

        if (request()->metode == 'hapus') {

            $data = $headline->find(request()->id);
            $data->delete();

            toast('Berhasil Dihapus !','warning');
            return redirect()->back();
        }

        if (request()->metode == 'edit') {

            $data = $headline->find(request()->id);
        } else {
            $data = null;
        }

        if (request()->metode == 'update') {
            $data = $headline->find(request()->id);

            $data->keterangan_headlines = request()->headline;
            $data->contoh_headlines = request()->contoh;
            $data->jenis_headlines = request()->jenis;

            $data->save();

            toast('Berhasil Diupdate !','info');
            return redirect()->back();
        }

        return view('backend.headline.index', compact('jenis', 'headlines', 'data'));
    }

    public function dataupload()
    {
        if (request()->hasFile('file')) {

            // validasi
            // $this->validate(request()->get('file'), [
            //     'file' => 'required|mimes:csv,xls,xlsx'
            // ]);

            $dataimport = new HeadlinesImport;

            Excel::import($dataimport, request()->file('file'));

            $banyakdata = $dataimport->getRowCount();

            // alihkan halaman kembali
            return redirect()->back()
            ->withFlashSuccess('Upload File Excel Jadwal Berhasil. Total '.$banyakdata.' Data');
        } else {
            return redirect()->back()->withFlashSuccess('Upload Gagal');
        }
    }
}
