<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserListHeadlinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_list_headlines', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('uuid');
            $table->string('id_user');
            $table->string('nama_user_list_headlines')->nullable();
            $table->string('status_user_list_headlines')->default('AKTIF')->nullable();
            $table->string('jenis_user_list_headlines')->nullable(); // draft, koleksi, bisni
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_list_headlines');
    }
}
