<?php

namespace App\Models;

use Ramsey\Uuid\Exception\UnsupportedOperationException;
use Ramsey\Uuid\Uuid;
use Illuminate\Database\Eloquent\Model;

class UserKoleksi extends Model
{
    protected $fillable = [
        'uuid',
        'id_user',
        'isi_user_koleksi',
        'status_user_koleksi',
        'jenis_user_koleksi',
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            try {
                $model->uuid = Uuid::uuid4()->toString();
            } catch (UnsupportedOperationException $e) {
                abort(500, $e->getMessage());
            }
        });
    }
}
