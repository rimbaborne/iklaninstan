<!doctype html>
<html lang="{{ htmlLang() }}" @langrtl dir="rtl" @endlangrtl>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ appName() }} | @yield('title')</title>
    <meta name="description" content="@yield('meta_description', appName())">
    <meta name="author" content="@yield('meta_author', 'eID')">
    @yield('meta')

    @stack('before-styles')
    {{-- <link rel="dns-prefetch" href="//fonts.gstatic.com"> --}}
    {{-- <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet"> --}}
    <link href="{{ mix('css/backend.css') }}" rel="stylesheet">
    <livewire:styles />
    <style>
    /* .form-group>label {
        top: 18px;
        left: 6px;
        position: relative;
        background-color: white;
        padding: 0px 5px 0px 5px;
        font-size: 0.9em;
    }
    .form-group>label {
    bottom: 26px;
    left: 10px;
    position: relative;
    background-color: white;
    padding: 0px 1px 0px 1px;
    font-size: 0.9em;
    transition: 0.2s;
    pointer-events: none;
    color: #536176;
    font-weight: 500;
    letter-spacing: 0.07em;
    }

    .form-control:focus~label {
    bottom: 47px;
    font-size: 1.03em;
    }

    .form-control:valid~label {
    bottom: 47px;
    font-size: 1.03em;
    } */

    .no-border {
        border: 0px solid !important;
    }
    html:not([dir=rtl]) .form-check {
        padding-left: 0rem;
    }
    @media only screen and (min-width: 1025px) and (max-width: 2500px) and (orientation: landscape) {
        .login-page {
            position: fixed;
            top: 50%;
            left: 50%;
            /* bring your own prefixes */
            transform: translate(-50%, -50%);
        }
    }
    </style>
    @stack('after-styles')

    @include('includes.partials.ga')
</head>
<body style="background-color: #fff;">
    @include('includes.partials.read-only')
    @include('includes.partials.logged-in-as')
    {{-- @include('includes.partials.announcements') --}}

    <div id="app">
        {{-- @include('frontend.includes.nav') --}}

        <main class="container py-4">
            @include('includes.partials.messages')
            @yield('content')
        </main>
    </div><!--app-->

    @stack('before-scripts')
    <script src="{{ mix('js/manifest.js') }}"></script>
    <script src="{{ mix('js/vendor.js') }}"></script>
    <script src="{{ mix('js/backend.js') }}"></script>
    <livewire:scripts />
    @stack('after-scripts')
</body>
</html>
