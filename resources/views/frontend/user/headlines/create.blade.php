@extends('frontend.layouts.app')

@section('title','Headlines Generator')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <nav aria-label="breadcrumb" role="navigation">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="/headline-generator">Headlines Generator</a></li>
                <li class="breadcrumb-item active" aria-current="page">Create</li>
                </ol>
            </nav>
            <div class="row">
                <div class="col-12">
                    <div class="card overflow-hidden">
                        <div class="card-body d-flex align-items-center">
                            <form class="col-12" action="{{ route('frontend.user.headlines.store') }}" method="post">
                                @csrf
                                <div class="form-group">
                                    <input name="headline" class="form-control" type="text" placeholder="Nama Headline">
                                </div>
                                <div class="form-group">
                                    <input name="contoh" class="form-control" type="text" placeholder="Contoh Headline">
                                </div>
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
