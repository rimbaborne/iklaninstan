@extends('frontend.layouts.app')

@section('title', 'Bisnis')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <nav aria-label="breadcrumb" role="navigation">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Bisnis</li>
                </ol>
            </nav>
            <div class="card overflow-hidden">
                <div class="card-body">
                    <form class="row" action="{{ route('frontend.user.bisnis') }}" method="get">
                        <div class="col-2">
                            <select  onchange='if(this.value != 0) { this.form.submit(); }' name="paged" style="border: 1px solid #d8dbe0; border-radius: 5px; padding: 5px;">
                                <option>{{ $paged }}</option>
                                <option>---</option>
                                <option>5</option>
                                <option>10</option>
                                <option>25</option>
                                <option>50</option>
                                <option>100</option>
                                <option>150</option>
                            </select>
                        </div>

                        <div class="col-10 text-center">
                        </div>

                        {{-- <div class="col-md-1 text-center" style="padding-left: 0px">
                            <button class="btn btn-primary btn-block"><i class="fa fa-search"></i> Cari</button>
                        </div> --}}
                    </form>
                </div>
                <div class="card-body d-flex align-items-center" style=" overflow-x: scroll;">
                    <table class="table table-hover table-outline mb-0" style="min-width: 600px;"">
                        <thead class="thead-light">
                            <tr>
                                <th width="50" class="text-center">
                                    <i class="nav-icon icon-chart"></i>
                                </th>
                                <th>Headline</th>
                                <th>Waktu</th>
                                <th width="250"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                            $first  = 0;
                            $end    = 0;
                            $number = 1;
                            @endphp
                            @foreach ($listheadlines as $key => $list)
                            <tr>
                                <td class="text-center">
                                    <div class="c-avatar">
                                        <strong>{{ $key + $listheadlines->firstItem() }}</strong>
                                        {{-- <span class="c-avatar-status bg-success"></span> --}}
                                    </div>
                                </td>
                                <td>
                                    <div>Headline {{ $key + $listheadlines->firstItem() }}</div>
                                </td>
                                <td>
                                    {{ $list->updated_at->diffForHumans() }}
                                    <div class="small text-muted">{{ $list->created_at }} </div>
                                </td>
                                <td class="" id="">
                                    {{-- <a href="" class="btn btn-info btn-sm">
                                        <i class="fas fa-search"></i> Lihat
                                    </a>

                                    <a href="/edit" class="btn btn-primary btn-sm">
                                        <i class="fas fa-pencil-alt"></i> Edit
                                    </a>

                                    <form method="POST" action="" name="delete-item" class="d-inline">
                                        @csrf
                                        <input type="hidden" name="_method" value="delete">
                                        <button type="submit" class="btn btn-danger btn-sm">
                                            <i class="fas fa-trash"></i> Hapus
                                        </button>
                                    </form> --}}
                                </td>
                            </tr>
                            @php
                            $first  = $listheadlines->firstItem();
                            $end    = $key + $listheadlines->firstItem();
                            @endphp
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-7">
                            <div class="float-left">
                                {!! $first !!} - {!! $end !!} Dari {!! $listheadlines->total() !!} Data
                            </div>
                        </div>

                        <div class="col-5">
                            <div class="float-right">
                                {!! $listheadlines->appends(request()->query())->links() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!--col-md-10-->
    </div><!--row-->
</div><!--container-->
@stack('before-scripts')

<script type="text/javascript">
    $(document).ready(function() {

    });
</script>

@stack('after-scripts')
@endsection
