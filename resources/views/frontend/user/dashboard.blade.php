@extends('frontend.layouts.app')

@section('title', __('Dashboard'))

@section('content')
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/flickity/1.0.0/flickity.css">
<style>
    .flickity-prev-next-button {
        width: 20px;
        height: 20px;
        background: hsla(0, 0%, 84%, 0.85);
    }
    .flickity-page-dots .dot {
        width: 12px;
        height: 12px;
    }
    .flickity-page-dots {
        bottom: 1145px;
    }
</style>
<script src="https://cdnjs.cloudflare.com/ajax/libs/flickity/1.0.0/flickity.pkgd.js"></script>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="row">
                {{-- <div class="col-12 col-lg-6">
                    <a href="#" style="text-decoration:none">
                        <div class="card overflow-hidden ro">
                            <div class="row text-center card-body d-flex align-items-center">
                                <div class="col-lg-6 text-info p-2" style="font-size: 25px">
                                    <i class="nav-icon icon-notebook"></i>
                                </div>
                                <div class="col-lg-6">
                                    <div class="text-value text-info">{{ $headlinekoleksi ?? 0 }}</div>
                                    <div class="text-muted text-uppercase font-weight-bold small">Koleksi</div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="col-12 col-lg-6">
                    <a href="#" style="text-decoration:none">
                        <div class="card overflow-hidden ro">
                            <div class="row text-center card-body d-flex align-items-center">
                                <div class="col-lg-6 text-warning p-2" style="font-size: 25px">
                                    <i class="nav-icon icon-chart"></i>
                                </div>
                                <div class="col-lg-6">
                                    <div class="text-value text-warning">{{ $headlinebisnis ?? 0 }}</div>
                                    <div class="text-muted text-uppercase font-weight-bold small">Bisnis</div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div> --}}

                <div class="col-md-10 pb-4">
                    @if (auth()->user()->active == false)
                        <div class="alert alert-danger" role="alert">
                            Akun Anda menunggu verifikasi. <br>
                            Setelah itu, maka Anda akan bisa menggunakan seluruh fitur-fitur diatas.
                        </div>
                    @else
                        <h2 class="display">
                            Selamat Datang,
                        </h2>
                        <h3 class="display" style="color: #0F75BC; font-weight: 700">
                            {{ auth()->user()->name }}
                        </h3>
                    @endif
                </div><!--col-md-10-->

                <div class="col-12">
                    <div class="row">
                        <div class="col-md-6 pb-4">
                            <div class="js-flickity" data-flickity-options='{ "wrapAround": true }'>
                                <img src="/img/header/1.png" class="img-fluid" alt="">
                                <img src="/img/header/2.png" class="img-fluid" alt="">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-6 col-4">
                                    <a href="{{ auth()->user()->active ? '/headline-generator' : '#'}}" class="card ro">
                                        <div class="card-body text-center" style="color: white; background: linear-gradient(45deg, #45a2ff 0%, #1869ab 100%) !important; border: solid 0px; border-radius: 10px">
                                            <i class="fas fa-edit fa-lg"></i>
                                            <div>
                                                Headline Generator
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-md-6 col-4">
                                    <a href="{{ auth()->user()->active ? '/copywriting-generator' : '#'}}" class="card ro">
                                        <div class="card-body text-center" style="color: white; background: linear-gradient(45deg, #740000 0%, #3e2b2b 100%) !important; border: solid 0px; border-radius: 10px">
                                            <i class="fas fa-edit fa-lg" ></i>
                                            <div>
                                               Copywriting Generator
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-md-6 col-4">
                                    <a href="#" class="card ro" onclick="alert('Feature Baru Dalam Pengembangan');">
                                        <div class="card-body text-center" style="color: rgb(41, 41, 41);">
                                            <i class="fas fa-edit fa-lg"></i>
                                            <div>
                                                Coming Soon
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>

    </div><!--row-->
</div><!--container-->
@endsection
