<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Headline;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class HeadlineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function simpan(Request $request)
    {
        $headline = new Headline;

        $headline->keterangan_headlines = $request->headline;
        $headline->contoh_headlines = $request->contoh;
        $headline->status_headlines = 'AKTIF';
        $headline->kategori_headlines = 'PERTAMA'; //masi difikirkan, intinya kategori per event
        $headline->jenis_headlines = 'UMUM';  //nanti bisa jenis headline fashion, makanan, properti, dll
        $headline->sorting_headlines = null;  //dipake untuk urutan, tp belum kepikiran

        $headline->save();

        return redirect()->back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('frontend.user.headlines.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $headline = new Headline;

        $headline->keterangan_headlines = $request->headline;
        $headline->contoh_headlines = $request->contoh;
        $headline->status_headlines = 'AKTIF';
        $headline->kategori_headlines = 'PERTAMA'; //masi difikirkan, intinya kategori per event
        $headline->jenis_headlines = 'UMUM';  //nanti bisa jenis headline fashion, makanan, properti, dll
        $headline->sorting_headlines = null;  //dipake untuk urutan, tp belum kepikiran

        $headline->save();

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Headline  $headline
     * @return \Illuminate\Http\Response
     */
    public function show(Headline $headline)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Headline  $headline
     * @return \Illuminate\Http\Response
     */
    public function edit(Headline $headline)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Headline  $headline
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Headline $headline)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Headline  $headline
     * @return \Illuminate\Http\Response
     */
    public function destroy(Headline $headline)
    {
        //
    }
}
