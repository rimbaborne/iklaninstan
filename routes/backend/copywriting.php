<?php

use App\Http\Controllers\Backend\CopywritingController;
use Tabuna\Breadcrumbs\Trail;

Route::get('copywriting', [CopywritingController::class, 'index'])
    ->name('copywriting')
    ->breadcrumbs(function (Trail $trail) {
        $trail->push('Copywriting', route('admin.copywriting'));
    });

Route::get('copywriting/generator', [CopywritingController::class, 'generator'])
    ->name('copywriting.generator')
    ->breadcrumbs(function (Trail $trail) {
        $trail->push('Copywriting / Generator', route('admin.copywriting.generator'));
    });

Route::get('copywriting/generator/data', [CopywritingController::class, 'generatordata'])
    ->name('copywriting.generator.data');

Route::post('copywriting/generator/data/upload', [CopywritingController::class, 'generatordataupload'])
    ->name('copywriting.generator.data.upload');
