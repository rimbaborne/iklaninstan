<?php

use App\Domains\Auth\Models\User;
use App\Http\Controllers\Frontend\HomeController;
use App\Http\Controllers\Frontend\TermsController;
use App\Http\Controllers\Frontend\User\AccountController;
use App\Http\Controllers\Frontend\User\DashboardController;
use App\Http\Controllers\Frontend\User\ProfileController;
use App\Http\Controllers\Frontend\HeadlineController;
use App\Http\Controllers\Frontend\CopywritingController;
use App\Http\Controllers\Frontend\UserHeadlineController;
use App\Http\Controllers\Frontend\UserCopywritingController;


/*
 * Frontend Controllers
 * All route names are prefixed with 'frontend.'.
 */

Route::get('/', [HomeController::class, 'index'])->name('index');
Route::get('terms', [TermsController::class, 'index'])->name('pages.terms');

/*
 * These frontend controllers require the user to be logged in
 * All route names are prefixed with 'frontend.'
 * These routes can not be hit if the user has not confirmed their email
 */
Route::group(['as' => 'user.', 'middleware' => ['auth', 'password.expires', config('boilerplate.access.middleware.verified')]], function () {
    Route::get('dashboard', [DashboardController::class, 'index'])
        ->middleware('type:' . User::TYPE_USER)
        ->name('dashboard');

    Route::get('account', [AccountController::class, 'index'])->name('account');
    Route::patch('profile/update', [ProfileController::class, 'update'])->name('profile.update');

    Route::get('headline-generator',                    [UserHeadlineController::class, 'index'])->name('headlines');
    Route::post('headline-generator/simpan',            [UserHeadlineController::class, 'simpan'])->name('headlines.simpan');
    Route::post('headline-generator/buat-iklan/temp',   [UserHeadlineController::class, 'buatiklantemp'])->name('headlines.buatiklan.temp');
    Route::get('headline-generator/buat-iklan',         [UserHeadlineController::class, 'buatiklan'])->name('headlines.buatiklan');
    Route::post('headline-generator/buat-iklan/simpan', [UserHeadlineController::class, 'buatiklansimpan'])->name('headlines.buatiklansimpan');

    Route::get('copywriting-generator',                       [UserCopywritingController::class, 'index'])->name('copywritings');
    Route::post('copywriting-generator/buat/temp', [UserCopywritingController::class, 'buatcopywritingtemp'])->name('copywritings.buatcopywriting.temp');
    Route::get('copywriting-generator/buat',      [UserCopywritingController::class, 'buatcopywriting'])->name('copywritings.buatcopywriting');

    Route::get('bisnis',            [DashboardController::class, 'bisnis'])->name('bisnis');
    Route::get('koleksi',           [DashboardController::class, 'koleksi'])->name('koleksi');
    Route::post('koleksi/hapus',    [DashboardController::class, 'koleksihapus'])->name('koleksi.hapus');

    //input -> khusus admin
    Route::get('copywriting-generator/create', [CopywritingController::class, 'create'])->name('copywritings.create');
    Route::post('copywriting-generator/store', [CopywritingController::class, 'store'])->name('copywritings.store');
    // Route::get('headline-generator/create', [HeadlineController::class, 'create'])->name('headlines.create');
    // Route::post('headline-generator/store', [HeadlineController::class, 'store'])->name('headlines.store');
});
