<?php

namespace App\Imports;

use App\Models\Copywriting;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Illuminate\Support\Str;

class CopywritingsImport implements ToModel, WithStartRow
{

    use Importable;
    private $rows = 0;
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        ++$this->rows;

        return new Copywriting([
            'uuid'                    => Str::uuid(),
            'keterangan_copywritings' => @$row[1],
            'contoh_copywritings'     => @$row[2],
            'jenis_copywritings'      => @$row[3],
        ]);
    }

    public function startRow(): int
    {
        return 1;
    }

    // public function limit(): int
    // {
    //     return 392;
    // }

    public function getRowCount(): int
    {
        return $this->rows;
    }
}
