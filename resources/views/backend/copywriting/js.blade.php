<script type = "text/javascript" >

    tinymce.init({
        selector: 'textarea',
        toolbar: 'mybutton | undo redo | bold italic underline strikethrough',
        menubar: false,
        setup: function(editor) {
            editor.ui.registry.addSplitButton('mybutton', {
                text: 'Menu Copywriting',
                onAction: function() {},
                onItemAction: function(api, value) {
                    editor.insertContent(value);
                },
                fetch: function(callback) {
                    if (editor.getContent() !== "") {

                        var items = {!! json_encode($datacp) !!};

                        callback(items);
                    };
                }
            });
        }
    });

</script>
