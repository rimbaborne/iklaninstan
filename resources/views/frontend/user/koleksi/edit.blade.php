@extends('frontend.layouts.app')

@section('title','Buat Iklan')

@section('content')
@stack('before-style')
<style>
    .card-footer {
        padding: 7px 20px 7px 20px;
    }
    textarea.form-control {
        border: none;
        color: #3c4b64;
        font-size: large;
        font-weight: 500;
    }
</style>
@stack('after-style')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <nav aria-label="breadcrumb" role="navigation">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="/headline-generator">Koleksi</a></li>
                <li class="breadcrumb-item active" aria-current="page">Eidt</li>
                </ol>
            </nav>
            <div class="row">
                <div class="col-12">p
                    <form class="card overflow-hidden" method="POST" action="{{ route('frontend.user.headlines.buatiklansimpan') }}">
                        @csrf
                        <div class="card-body d-flex align-items-center contain-iklan" style="">
                            <input hidden id="i{{ ++$n }}" value="{{ $headline[$i] }}">
                            <textarea id="t{{ $n }}" name="headline" class="form-control">{{ $headline[$i] }}</textarea>
                        </div>
                        <div class="card-footer">
                            <div class="float-right">
                                <button type="button" onclick="reset(i{{ $n }}, t{{ $n }})" class="btn btn-warning btn-pill pl-3 pr-3 btn-sm">
                                    <i class="nav-icon icon-action-undo"></i>
                                    Reset
                                </button>
                                <button data-clipboard-target="#t{{ $n }}" type="button" class="btn btn-info btn-pill pl-3 pr-3 btn-sm">
                                    <i class="nav-icon icon-layers"></i>
                                    Salin
                                </button>
                                <button type="submit" class="btn btn-primary btn-pill pl-3 pr-3 btn-sm">
                                    <i class="nav-icon icon-note"></i>
                                    Simpan
                                </button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div><!--col-md-10-->
    </div><!--row-->
</div><!--container-->
@stack('before-scripts')
{{-- <script type='text/javascript' src="https://www.jqueryscript.net/demo/Copy-Text-To-Clipboard-jQuery-copiq/jquery.copiq.js"></script> --}}
{{-- <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script> --}}
<script type='text/javascript' src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/clipboard@2.0.6/dist/clipboard.min.js"></script>

<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(document).ready(function() {
        function reset(idreset, idtextarea){
            document.getElementById(idtextarea).value = document.getElementById(idreset).value;
        }

        var clipboard = new ClipboardJS('.btn');

        clipboard.on('success', function(e) {
            alert(e.text + ' - Berhasil Tersalin !');
        });

        // clipboard.on('error', function(e) {
        //     console.error('Action:', e.action);
        //     console.error('Trigger:', e.trigger);
        // });

    });

    function simpan(idtextarea){
        $.ajax({
                method: 'POST',
                url: '{{ route("frontend.user.headlines.simpan") }}',
                data: {
                    "_token": "{{ csrf_token() }}",
                        headline: "tes"
                    },
                success: function () {
                    console.info('Berhasil', e.action);
                }
            })

        }


</script>
{{-- @for ($i = 1; $i <= 50; $i++)
$('{{ '#b'.$i }}').copiq({
    content: '{{ '#t'.$i }}',
    onSuccess: function() {
        alert("Berhasil Tersalin !");
        return false;
    }
});
@endfor --}}
@stack('after-scripts')
@endsection
