<?php

namespace App\Models;

use Ramsey\Uuid\Exception\UnsupportedOperationException;
use Ramsey\Uuid\Uuid;
use Illuminate\Database\Eloquent\Model;

class Copywriting extends Model
{
    protected $fillable = [
        'uuid',
        'keterangan_copywritings',
        'contoh_copywritings',
        'status_copywritings',
        'kategori_copywritings',
        'jenis_copywritings'
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            try {
                $model->uuid = Uuid::uuid4()->toString();
            } catch (UnsupportedOperationException $e) {
                abort(500, $e->getMessage());
            }
        });
    }
}
