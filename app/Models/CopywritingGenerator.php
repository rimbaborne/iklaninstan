<?php

namespace App\Models;

use Ramsey\Uuid\Exception\UnsupportedOperationException;
use Ramsey\Uuid\Uuid;
use Illuminate\Database\Eloquent\Model;

class CopywritingGenerator extends Model
{
    protected $fillable = [
        'uuid',
        'nama_copywriting_generators',
        'data_copywriting_generators',
        'status_copywriting_generators',
        'kategori_copywriting_generators',
        'jenis_copywriting_generators'
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            try {
                $model->uuid = Uuid::uuid4()->toString();
            } catch (UnsupportedOperationException $e) {
                abort(500, $e->getMessage());
            }
        });
    }
}
