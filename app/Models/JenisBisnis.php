<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class JenisBisnis extends Model
{
    protected $fillable = [
        'nama',
        'keterangan',
        'kategori',
    ];
}
