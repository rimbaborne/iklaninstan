@extends('backend.layouts.app')

@section('title', 'Headline')

@section('content')
<div class="fade-in">
    <div class="card">
        <div class="card-body">
            <form onmouseover="verifikasi()" class="form-horizontal col-md-12" action="{{ route('admin.headline.upload') }}" method="POST" enctype="multipart/form-data" style="padding-top: 20px">
                <div class="form-group row" style="margin-bottom:0px">
                    {{ csrf_field() }}
                    <label class="col-md-1 col-form-label" for="file-input">
                        Pilih File  :
                    </label>
                    <div class="col-md-5">
                        <input type="file" name="file" class="form-control" id="upload" required>
                    </div>
                    <div class="col-md-2">
                        <button id="btnupload" type="submit" class="btn btn-primary btn-block" >Upload File</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="card">
        <div class="card-header">
            Data Headline
        </div><!--card-header-->
        <div class="card-body">
            @if (request()->metode == 'edit')
                <form class="col-12" action="#">
                    @csrf
                    <input type="hidden" name="metode" value="update">
                    <input type="hidden" name="id" value="{{ $data->id }}">
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Nama Headline</label>
                        <div class="col-sm-10">
                            <input name="headline" value="{{ $data->keterangan_headlines }}" class="form-control" type="text" placeholder="Edit Nama Headline" value="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Contoh Headline</label>
                        <div class="col-sm-10">
                            <input name="contoh" value="{{ $data->contoh_headlines }}" class="form-control" type="text" placeholder="Edit Contoh Headline">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Jenis Headline</label>
                        <div class="col-sm-4">
                            <select id="jenis" name="jenis" class="form-control">
                                <option value="{{ $data->jenis_headlines }}">{{ $data->jenis_headlines }}</option>
                                <option value="">-----</option>
                                @foreach ($jenis as $jns)
                                    <option value="{{ $jns->nama }}">{{ $jns->nama }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <a href="{{ route('admin.headline') }}" class="btn btn-light">Kembali</a>

                    <button type="submit" class="btn btn-primary">Update</button>
                </form>
            @else
                <form class="col-12" action="#">
                    @csrf
                    <input type="hidden" name="metode" value="tambah">
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Nama Headline</label>
                        <div class="col-sm-10">
                            <input name="headline" class="form-control" type="text" >
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Contoh Headline</label>
                        <div class="col-sm-10">
                            <input name="contoh" class="form-control" type="text">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Jenis Headline</label>
                        <div class="col-sm-4">
                            <select id="jenis" name="jenis" class="form-control">
                                @foreach ($jenis as $jns)
                                    <option value="{{ $jns->nama }}">{{ $jns->nama }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </form>
        </div><!--card-body-->
    </div><!--card-->
    <div class="card">
        <div class="card-body">
            <div class="col-12">
                <table class="table table-sm">
                    <thead>
                        <th class="text-center">No</th>
                        <th>Nama Headline</th>
                        <th>Jenis</th>
                        <th width="150"></th>
                    </thead>
                    <tbody>
                        @foreach ($headlines as $key => $headline)
                        <tr>
                            <td class="text-center">{{ $key + $headlines->firstItem() }}</td>
                            <td>
                                {{ $headline->keterangan_headlines }}
                                <div class="text-muted">
                                    Contoh: {{ $headline->contoh_headlines }}
                                </div>
                            </td>
                            <td>{{ $headline->jenis_headlines }}</td>
                            <td>
                                <div class="btn-group" role="group" aria-label="Default button group">
                                    <a href="{{ route('admin.headline') }}/?metode=edit&id={{ $headline->id }}" class="btn btn-sm btn-success" type="button">Edit</a>
                                    <form action="#" onsubmit="return confirm('Apakah Yakin Dihapus ?');">
                                        @csrf
                                        <input type="hidden" name="metode" value="hapus">
                                        <input name="id" value="{{ $headline->id }}" hidden>
                                        <button class="btn btn-sm btn-danger" >Hapus</button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                        @php
                        $first = $headlines->firstItem();
                        $end = $key + $headlines->firstItem();
                        @endphp
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="col-12">
                <div class="row" style="padding-top:10px">
                    <div class="col-7">
                        <div class="float-left" style="padding-left:10px">
                            {!! $first !!} - {!! $end !!} Dari {!! $headlines->total() !!} Data
                        </div>
                    </div>
                    <!--col-->

                    <div class="col-5">
                        <div class="float-right" style="padding-right:10px">
                            {!! $headlines->appends(request()->query())->links() !!}
                        </div>
                    </div>
                    <!--col-->
                </div>
            </div>
            @endif

        </div><!--card-body-->
    </div><!--card-->
</div>
@endsection
