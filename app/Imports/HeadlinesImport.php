<?php

namespace App\Imports;

use App\Models\Headline;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Illuminate\Support\Str;

class HeadlinesImport implements ToModel, WithStartRow
{

    use Importable;
    private $rows = 0;
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        ++$this->rows;

        return new Headline([
            'uuid'                 => Str::uuid(),
            'keterangan_headlines' => @$row[1],
            'contoh_headlines'     => @$row[2],
            'jenis_headlines'      => @$row[3],
        ]);
    }

    public function startRow(): int
    {
        return 1;
    }

    // public function limit(): int
    // {
    //     return 392;
    // }

    public function getRowCount(): int
    {
        return $this->rows;
    }
}
