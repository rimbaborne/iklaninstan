<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCopywritingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('copywritings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('uuid');
            $table->string('keterangan_copywritings')->nullable();
            $table->string('contoh_copywritings')->nullable();
            $table->string('status_copywritings')->default('AKTIF')->nullable();
            $table->string('kategori_copywritings')->nullable();
            $table->string('jenis_copywritings')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('copywritings');
    }
}
