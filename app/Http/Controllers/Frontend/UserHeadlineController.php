<?php

namespace App\Http\Controllers\Frontend;

use App\Models\UserHeadline;
use App\Models\UserListHeadline;
use App\Models\Headline;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Carbon;
use App\Models\UserKoleksi;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Cookie;
use App\Models\JenisBisnis;


class UserHeadlineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $headlines = Headline::where('status_headlines', 'AKTIF')
                                ->where('jenis_headlines', 'UMUM')
                                ->get();

        if (auth()->user()->active == false) {
            return redirect()->route('frontend.user.dashboard');
        }

        $user = Cookie::get('user');
        $user_ = json_decode($user);

        $jenis = JenisBisnis::orderBy('nama')->get();

        return view('frontend.user.headlines.index', compact('headlines', 'user', 'jenis'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function simpan(Request $request)
    {
        $userheadline     = new UserHeadline;
        $tanggalan        = Carbon::now();

        // buat jenis headline per 1 kali simpan headline, trus dapetin idnya untuk diapaikai di perjawaban headline
        $idlist = DB::table('user_list_headlines')->insertGetId(
            [
                "uuid" => Uuid::uuid4()->toString(),
                "id_user" => auth()->user()->id,
                "status_user_list_headlines" => 'AKTIF',
                "jenis_user_list_headlines" => 'BISNIS',
                "created_at" => $tanggalan,
                "updated_at" => $tanggalan,
            ]
        );

        $headlines = $request->headline;
        $n         = 0;
        $data      = array();


        // penyimpanan form input untuk menu bisni, satu persatu
        foreach ($headlines as $h) {
            $data[] = [
                "uuid"                   => Uuid::uuid4()->toString(),
                "id_headlines"           => $request->idheadline[$n++],
                "id_user"                => auth()->user()->id,
                "id_list_headlines"      => $idlist,
                "jawaban_user_headlines" => $h,
                "created_at"             => $tanggalan,
                "updated_at"             => $tanggalan,
            ];
        }
        DB::table('user_headlines')->insert($data);

        return redirect()->route('frontend.user.bisnis');
        // dd($data);
    }

    //disimpan temporary dulu
    public function buatiklantemp(Request $request)
    {
        $tanggalan        = Carbon::now();

        // ngambil data request
        $headlines = $request->headline ?? null;
        $n         = 0;
        $data      = array();

        foreach ($headlines as $h) {
            $data[] = [
                "id_headlines" => $request->idheadline[$n++],
                "jawaban_user_headlines" => $h,
            ];
        }

        // data template headline dijadikan array
        $headline = array(
            'Bagaimana ' . $data[0]['jawaban_user_headlines'] . ' Tanpa ' . $data[2]['jawaban_user_headlines'],
            'Sekarang Anda bisa ' . $data[0]['jawaban_user_headlines'] . ' sambil ' . $data[3]['jawaban_user_headlines'],
            $data[4]['jawaban_user_headlines'] . ' orang sudah ' . $data[5]['jawaban_user_headlines'] . ' siapa lagi yang mau ' . $data[0]['jawaban_user_headlines'] . ' ?',
            'Untuk Anda yang ingin ' . $data[6]['jawaban_user_headlines'],
            'Hanya ' . $data[3]['jawaban_user_headlines'] . '  bisa ' . $data[0]['jawaban_user_headlines'],
            'Jangan ' . $data[7]['jawaban_user_headlines'] . ' sebelum ' . $data[5]['jawaban_user_headlines'],
            'Cara Saya ' . $data[6]['jawaban_user_headlines'] . ' yang bisa Anda tiru ',
            'Sudah ' . $data[5]['jawaban_user_headlines'] . ' belum ' . ' ?',
            $data[8]['jawaban_user_headlines'] . ' hanya ' . $data[9]['jawaban_user_headlines'],
            $data[10]['jawaban_user_headlines'] . ' untuk kamu ',
            $data[11]['jawaban_user_headlines'] . ' Tips Praktis untuk ' . $data[0]['jawaban_user_headlines'] .  ' Walaupun ' .  $data[2]['jawaban_user_headlines'],
            $data[12]['jawaban_user_headlines'] . ' Rekomendasi ' . $data[13]['jawaban_user_headlines'] . ' untuk ' . $data[0]['jawaban_user_headlines'] . ' dalam ' . $data[14]['jawaban_user_headlines'],
            $data[12]['jawaban_user_headlines'] . '  yang cocok untuk ' . $data[15]['jawaban_user_headlines'] . ' Tanpa ' .  $data[2]['jawaban_user_headlines'],
            'Tiru Cara ' . $data[13]['jawaban_user_headlines'] . ' ' . $data[6]['jawaban_user_headlines'],
            'Siapa lagi yang mau ' . $data[0]['jawaban_user_headlines'] . ' dalam ' . $data[14]['jawaban_user_headlines'] . ' ?',
            $data[4]['jawaban_user_headlines'] . ' orang sudah ' . $data[5]['jawaban_user_headlines'] . '  Yuk  ikutan juga ',
            'Apakah Anda : ' . $data[15]['jawaban_user_headlines'] . ' yang ingin ' . $data[5]['jawaban_user_headlines'] . ' ?',
            'Inilah cara yang terbukti ampuh ' . $data[0]['jawaban_user_headlines'] . '  tanpa ' . $data[2]['jawaban_user_headlines'],
            'Sekarang Anda bisa ' . $data[0]['jawaban_user_headlines'] . '  dengan cara ini ',
            $data[11]['jawaban_user_headlines'] . ' cara mudah yang bisa Anda lakukan agar ' . $data[0]['jawaban_user_headlines'],
            'Saya bocorkan ke Anda ' . $data[0]['jawaban_user_headlines'],
            'Cara ' . $data[6]['jawaban_user_headlines'] . ' ' .  $data[3]['jawaban_user_headlines'],
            $data[15]['jawaban_user_headlines'] . ', sekarang Anda bisa ' . $data[6]['jawaban_user_headlines'] . ' tanpa ' .  $data[2]['jawaban_user_headlines'],
            $data[11]['jawaban_user_headlines'] . ' tips untuk ' . $data[0]['jawaban_user_headlines'] . ' bagi  ' . $data[15]['jawaban_user_headlines'],
            'Rahasia ' . $data[0]['jawaban_user_headlines'] . '  Tanpa ' . $data[2]['jawaban_user_headlines'],
            'Begini cara agar terhindar dari ' . $data[1]['jawaban_user_headlines'],
            'Cara tercepat ' .  $data[0]['jawaban_user_headlines'] . ' yang jarang orang ketahui ',
            $data[11]['jawaban_user_headlines'] . ' jurus ampuh ' . $data[0]['jawaban_user_headlines'],
            'Tutorial lengkap untuk ' . $data[6]['jawaban_user_headlines'] . ' seperti impian Anda ',
            'Inilah resep yang bisa membantu ' . $data[15]['jawaban_user_headlines'] . ' untuk ' . $data[0]['jawaban_user_headlines'],
            'Jangan dulu ' . $data[7]['jawaban_user_headlines'] . ' sebelum tau hal ini ',
            'Gelisah karena ' . $data[1]['jawaban_user_headlines'] . ' Mungkin INI alasannya ',
            'Untuk yang ingin belajar ' . $data[16]['jawaban_user_headlines'] . ' agar ' . $data[0]['jawaban_user_headlines'],
            'Spesial untuk ' . $data[15]['jawaban_user_headlines'] . ', bongkar blak-blakan SEMUA rahasia ' . $data[0]['jawaban_user_headlines'],
            $data[11]['jawaban_user_headlines'] . ' yang membuat ' . $data[1]['jawaban_user_headlines'] . ' dan Solusi praktisnya ',
            'Untuk Anda ' . $data[15]['jawaban_user_headlines'] . ' , hal yang ingin ' . $data[6]['jawaban_user_headlines'],
            'Ini Rahasia ' . $data[0]['jawaban_user_headlines'] . ' Tanpa ' . $data[2]['jawaban_user_headlines'],
            'Bagaimana ' . $data[0]['jawaban_user_headlines'] . ' dalam ' . $data[14]['jawaban_user_headlines'],
            'Modal ' . $data[17]['jawaban_user_headlines'] . ' bisa ' . $data[16]['jawaban_user_headlines'],
            'Cara mudah Kuasai ' . $data[16]['jawaban_user_headlines'],
            'Bagaimana cara Saya mengubah ' . $data[1]['jawaban_user_headlines'] . ' menjadi ' . $data[0]['jawaban_user_headlines'],
            'Siapa lagi yang mau ' . $data[5]['jawaban_user_headlines'] . ' dan ' . $data[0]['jawaban_user_headlines'],
            'Anda bisa ' . $data[0]['jawaban_user_headlines'] . '  Dengan ini ',
            'Ajaib ! Hanya dengan ' . $data[17]['jawaban_user_headlines'] . ' siapa saja bisa ' . $data[6]['jawaban_user_headlines'],
            'Ampuh, baru ' . $data[14]['jawaban_user_headlines'] . ' sudah ' . $data[0]['jawaban_user_headlines'],
            'Cara mudah mengatasi ' . $data[1]['jawaban_user_headlines'],
            'Saya mau bagikan tips ' . $data[16]['jawaban_user_headlines'] . ' yang selama ini Saya rahasiakan, Mau ?',
            $data[11]['jawaban_user_headlines'] . ' hal yang perlu Anda ketahui tentang ' . $data[16]['jawaban_user_headlines'],
            $data[11]['jawaban_user_headlines'] . ' alasan kenapa harus ' . $data[6]['jawaban_user_headlines'],
            'Jangan sampai merasakan ' . $data[1]['jawaban_user_headlines'],
        );

        // disimpan jadi json. nanti dikeluarin biar tetap baca array
        $headline = json_encode($headline);

        // cek dulu datanya tempnya. kalo sudah ada tinggal update, 1 user -> 1 temp
        $temp_iklan = DB::table('temp_user_iklans')->where('id_user', auth()->user()->id)->first();

        if (isset($temp_iklan)) {
            DB::table('temp_user_iklans')->where('id_user', auth()->user()->id)->update([
                "iklan"      => $headline,
                "updated_at" => $tanggalan
            ]);
        } else {
            DB::table('temp_user_iklans')->insert(
                [
                    "uuid"       => Uuid::uuid4()->toString(),
                    "id_user"    => auth()->user()->id,
                    "iklan"      => $headline,
                    "created_at" => $tanggalan,
                    "updated_at" => $tanggalan
                ]
            );
        }
        return redirect()->route('frontend.user.headlines.buatiklan');
    }

    // ngambil data dari temporary
    public function buatiklan(Request $request)
    {
        // ngambil data dari temp
        $headline_ =  DB::table('temp_user_iklans')->where('id_user', auth()->user()->id)->first();
        if (!isset($headline_)) {
            return redirect()->route('frontend.user.headlines');
        }
        $headline  = json_decode($headline_->iklan);



        return view('frontend.user.headlines.buatiklan', compact('headline'));
    }

    //simpan satu persatu headline di koleksi
    public function buatiklansimpan(Request $request)
    {
        $tanggalan        = Carbon::now();

        //memperbaruhi temp iklan headline biar update
        $headline_ =  DB::table('temp_user_iklans')->where('id_user', auth()->user()->id)->first();
        $headline  = json_decode($headline_->iklan);

        $headline[$request->input('idheadline')] = $request->input('isiheadline');
        $headline  = json_encode($headline);

        DB::table('temp_user_iklans')->where('id_user', auth()->user()->id)->update([
            "iklan"      => $headline,
            "updated_at" => $tanggalan
        ]);

        // masukkan data ke koleksi
        UserKoleksi::insert(
            [
                "uuid" => Uuid::uuid4()->toString(),
                "id_user" => auth()->user()->id,
                "isi_user_koleksi" => $request->input('isiheadline'),
                "status_user_koleksi" => 'AKTIF',
                "jenis_user_koleksi" => 'HEADLINE',
                "created_at" => $tanggalan,
                "updated_at" => $tanggalan,
            ]
        );

        alert()->html('<div>Penyimpanan Berhasil !', "<a style='font-size: 12px; font-weight: 600' href='/koleksi'>Ke Halaman Penyimpanan Koleksi <i class='fas fa-chevron-right'></i></a>", 'success')
            ->showConfirmButton('Tetap Dihalaman Headline', '#3085d6')
            ->padding('10px')
            ->showCloseButton();

        return redirect()->back();

        // return response()->json(['success' => 'Product saved successfully.']);

        // return 'Berhasil';

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\UserHeadline  $userHeadline
     * @return \Illuminate\Http\Response
     */
    public function show(UserHeadline $userHeadline)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\UserHeadline  $userHeadline
     * @return \Illuminate\Http\Response
     */
    public function edit(UserHeadline $userHeadline)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\UserHeadline  $userHeadline
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserHeadline $userHeadline)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\UserHeadline  $userHeadline
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserHeadline $userHeadline)
    {
        //
    }
}
