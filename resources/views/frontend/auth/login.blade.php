@extends('frontend.layouts.app-a')

@section('title', __('Login'))

@section('content')
<div class="row justify-content-center login-page">
    <div class="col-md-12">
        <div class="card-group">
            <div class="card text-white no-border">
                <div class="card-body text-center">
                    <div class="row justify-content-center">
                        <div class="col-6 col-md-9" style="padding-top: 100px">
                            <img src="/img/logo-lg.png" alt="" class="img-responsive img-fluid">
                        </div>
                    </div>
                </div>
            </div>
            <div class="card p-4 no-border">
                <div class="card-body">
                    {{-- <h1>Login</h1>
                    <p class="text-muted">Masukkan Akun Anda</p> --}}
                    <x-forms.post :action="route('frontend.auth.login')">
                        <div class="form-group">
                            <label class="text-muted font-weight-bold">Email</label>
                            <input type="email" name="email" id="email" class="form-control" placeholder="{{ __('E-mail Address') }}" value="{{ old('email') }}" maxlength="255" required autofocus autocomplete="email" >
                        </div>
                        <div class="form-group">
                            <label class="text-muted font-weight-bold">Password</label>
                            <input type="password" name="password" id="password" class="form-control" placeholder="{{ __('Password') }}" maxlength="100" required autocomplete="current-password" >
                        </div>
                        <div class="form-group row">
                            <div class="col-6">
                            </div>
                            <div class="col-6 text-right">
                                <x-utils.link :href="route('frontend.auth.password.request')" class="btn btn-link" :text="__('Forgot Your Password?')" />
                            </div>
                        </div>


                        {{-- <div class="input-group mb-3">
                            <div class="input-group-prepend"><span class="input-group-text">
                                <i class="fas fa-user"></i>
                            </div>
                            <input type="email" name="email" id="email" class="form-control" placeholder="{{ __('E-mail Address') }}" value="{{ old('email') }}" maxlength="255" required autofocus autocomplete="email" />
                        </div>
                        <div class="input-group mb-4">
                            <div class="input-group-prepend"><span class="input-group-text">
                                <i class="fas fa-lock"></i>
                            </div>
                            <input type="password" name="password" id="password" class="form-control" placeholder="{{ __('Password') }}" maxlength="100" required autocomplete="current-password" />
                        </div> --}}
                        <div class="form-group row">
                            <div class="col-md-12 text-center">
                                <div class="form-check">
                                    <input  name="remember" id="remember" class="form-check-input" type="checkbox" {{ old('remember') ? 'checked' : '' }} />
                                    <label style="padding-left: 5px; margin-right:-15px" class="form-check-label" for="remember">
                                        @lang('Remember Me')
                                    </label>
                                    <div>
                                        <div class="pt-2">
                                            <button type="submit" class="btn btn-primary" style="padding: 5px 50px 5px 50px" type="button">Login</button>
                                        </div>
                                    </div>
                                </div><!--form-check-->
                            </div>
                        </div><!--form-group-->
                        @if(config('boilerplate.access.captcha.login'))
                        <div class="row">
                            <div class="col">
                                @captcha
                                <input type="hidden" name="captcha_status" value="true" />
                            </div><!--col-->
                        </div><!--row-->
                        @endif

                        <div class="row">
                            <div class="col-12">
                                <div class="text-muted font-weight-bold text-center">
                                    Belum punya akun? Silahkan <a href="/register">DAFTAR DISINI</
                                </div>
                            </div>
                        </div>
                    </x-forms.post>

                </div>
            </div>
        </div>
    </div>
</div>
{{-- <div class="row justify-content-center">
    <div class="col-md-8">
        <x-frontend.card>
            <x-slot name="header">
                @lang('Login')
            </x-slot>

            <x-slot name="body">
                <x-forms.post :action="route('frontend.auth.login')">
                    <div class="form-group row">
                        <label for="email" class="col-md-4 col-form-label text-md-right">@lang('E-mail Address')</label>

                        <div class="col-md-6">
                            <input type="email" name="email" id="email" class="form-control" placeholder="{{ __('E-mail Address') }}" value="{{ old('email') }}" maxlength="255" required autofocus autocomplete="email" />
                        </div>
                    </div><!--form-group-->

                    <div class="form-group row">
                        <label for="password" class="col-md-4 col-form-label text-md-right">@lang('Password')</label>

                        <div class="col-md-6">
                            <input type="password" name="password" id="password" class="form-control" placeholder="{{ __('Password') }}" maxlength="100" required autocomplete="current-password" />
                        </div>
                    </div><!--form-group-->

                    <div class="form-group row">
                        <div class="col-md-6 offset-md-4">
                            <div class="form-check">
                                <input name="remember" id="remember" class="form-check-input" type="checkbox" {{ old('remember') ? 'checked' : '' }} />

                                <label class="form-check-label" for="remember">
                                    @lang('Remember Me')
                                </label>
                            </div><!--form-check-->
                        </div>
                    </div><!--form-group-->

                    @if(config('boilerplate.access.captcha.login'))
                    <div class="row">
                        <div class="col">
                            @captcha
                            <input type="hidden" name="captcha_status" value="true" />
                        </div><!--col-->
                    </div><!--row-->
                    @endif

                    <div class="form-group row mb-0">
                        <div class="col-md-8 offset-md-4">
                            <button class="btn btn-primary" type="submit">@lang('Login')</button>

                            <x-utils.link :href="route('frontend.auth.password.request')" class="btn btn-link" :text="__('Forgot Your Password?')" />
                        </div>
                    </div><!--form-group-->

                    <div class="text-center">
                        @include('frontend.auth.includes.social')
                    </div>
                </x-forms.post>
            </x-slot>
        </x-frontend.card>
    </div><!--col-md-8-->
</div><!--row--> --}}
@endsection
