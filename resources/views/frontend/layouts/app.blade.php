<!doctype html>
<html lang="{{ htmlLang() }}" @langrtl dir="rtl" @endlangrtl>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ appName() }} | @yield('title')</title>
    <meta name="description" content="@yield('meta_description', appName())">
    <meta name="author" content="@yield('meta_author', 'eID')">
    @yield('meta')

    @stack('before-styles')
    {{-- <link rel="dns-prefetch" href="//fonts.gstatic.com"> --}}
    {{-- <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet"> --}}
    <link href="{{ mix('css/backend.css') }}" rel="stylesheet">
    <link href="{{ ('https://coreui.io/demo/2.0/vendors/simple-line-icons/css/simple-line-icons.css') }}" rel="stylesheet">

    {{-- <livewire:styles /> --}}
    <style>
        .ro {
            border-radius: 15px;
            box-shadow: 0px 5px 10px #66808d29;
        }
        .custom-shape-divider-top-1595183375 {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            overflow: hidden;
            line-height: 0;
        }

        .custom-shape-divider-top-1595183375 svg {
            position: relative;
            display: block;
            width: calc(100% + 1.3px);
            height: 33px;
        }

        .custom-shape-divider-top-1595183375 .shape-fill {
            fill: #FFFFFF8F;
        }
        .label-headlines {
            font-size: 18px;
        }
    </style>
    @stack('after-styles')

    @stack('before-scripts')
    <script src="{{ mix('js/backend.js') }}"></script>
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="{{ '/js/sweetalert.all.js' }}"></script>
    {{-- <livewire:scripts /> --}}
    @stack('after-scripts')

    @include('includes.partials.ga')
</head>
<body style="background-color: white">
    @include('includes.partials.read-only')
    @include('includes.partials.logged-in-as')
    @include('includes.partials.announcements')

    <div id="app">
        @include('frontend.includes.nav')

        <main class="container py-4">
            @include('includes.partials.messages')
            @include('sweetalert::alert')
            @yield('content')
        </main>
    </div><!--app-->

    @stack('before-scripts')
    <script src="{{ mix('js/manifest.js') }}"></script>
    <script src="{{ mix('js/vendor.js') }}"></script>
    {{-- <script src="{{ mix('js/backend.js') }}"></script> --}}

    @stack('after-scripts')
</body>
</html>
