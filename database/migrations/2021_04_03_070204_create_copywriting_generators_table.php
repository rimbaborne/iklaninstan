<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCopywritingGeneratorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('copywriting_generators', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('uuid');
            $table->string('nama_copywriting_generators')->nullable();
            $table->text('data_copywriting_generators')->nullable();
            $table->string('status_copywriting_generators')->default('AKTIF')->nullable();
            $table->string('kategori_copywriting_generators')->nullable();
            $table->string('jenis_copywriting_generators')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('copywriting_generators');
    }
}
