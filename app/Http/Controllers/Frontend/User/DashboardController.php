<?php

namespace App\Http\Controllers\Frontend\User;

use App\Http\Controllers\Controller;
use App\Models\UserListHeadline;
use ParagonIE\Sodium\Compat;
use App\Models\UserKoleksi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * Class DashboardController.
 */
class DashboardController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $headlinebisnis = UserListHeadline::where('id_user', auth()->user()->id)
            ->where('jenis_user_list_headlines', 'BISNIS')->count();
        $headlinekoleksi = UserListHeadline::where('id_user', auth()->user()->id)
            ->where('jenis_user_list_headlines', 'KOLEKSI')->count();
        $headlinedraft = UserListHeadline::where('id_user', auth()->user()->id)
            ->where('jenis_user_list_headlines', 'DRAFT')->count();

        return view('frontend.user.dashboard', compact('headlinebisnis', 'headlinekoleksi', 'headlinedraft'));
    }

    public function bisnis()
    {
        $paged = request()->input('paged') ?? 10;
        $listheadlines = UserListHeadline::where('id_user', auth()->user()->id)
            ->where('jenis_user_list_headlines', 'BISNIS')->paginate($paged);

        return view('frontend.user.bisnis.index', compact('listheadlines', 'paged'));
    }

    public function koleksi()
    {
        $cari    = request()->input('cari');
        $paged   = request()->input('paged') ?? 10;
        $listheadlines = UserKoleksi::where('id_user', auth()->user()->id)
            ->where('jenis_user_koleksi', 'HEADLINE')
            ->when($cari, function ($query, $cari) {
                return $query->where('isi_user_koleksi', 'like', '%' . $cari . '%');
            })
            ->paginate($paged);

        return view('frontend.user.koleksi.index', compact('listheadlines', 'paged'));
    }

    public function koleksihapus(Request $request)
    {
        try {
            $koleksi = DB::table('user_koleksis')->where('uuid', $request->input('uuid'))
                ->where('id_user', auth()->user()->id);
            $koleksi->delete();
            toast('Koleksi Iklan Headline, Berhasil Terhapus ! ', 'success');
        } catch (\Throwable $th) {
            toast('Proses Gagal, Gagal Terhapus ! ', 'warning');
        }

        return redirect()->back();
    }
}
