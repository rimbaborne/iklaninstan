@extends('backend.layouts.app')

@section('title', 'Copywriting | Generator')

@section('content')
@stack('before-scripts')
<script src="https://cdn.tiny.cloud/1/5g7yssamd0271mz2hhe998gnkav4u9v2a52inq9hul2j7qa4/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
@include('backend.copywriting.js')
@stack('after-scripts')
<div class="fade-in">
    <div class="card">
        <div class="card-body">
            @if (request()->metode == 'edit')
                <form class="col-12 pb-2" action="#">
                    @csrf
                    <input type="hidden" name="id" value="{{ $data->id }}">
                    <input type="hidden" name="metode" value="update">
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Nama Copywriting Generator</label>
                        <div class="col-sm-10">
                            <input name="nama" class="form-control" type="text" value="{{ $data->nama_copywriting_generators }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Jenis Copywriting</label>
                        <div class="col-sm-4">
                            <select id="jenis" name="jenis" class="form-control" disabled>
                                <option value="{{ $data->jenis_copywriting_generators }}">{{ $data->jenis_copywriting_generators }}</option>
                                <option value="">-----</option>
                                @foreach ($jenis as $jns)
                                    <option value="{{ $jns->nama }}">{{ $jns->nama }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-12">
                            <textarea name="data" id="data">{{ $data->data_copywriting_generators }}</textarea>
                        </div>
                    </div>
                    <a href="{{ route('admin.copywriting.generator') }}" class="btn btn-light">Kembali</a>

                    <button type="submit" class="btn btn-primary">Update</button>
                </form>
                <div class="col-sm-12">
                    <hr>
                </div>
                <div class="col-sm-12 pt-4">
                        <h2 class="display">Hasil</h2>
                        <div style="font-size: 14px">
                            {!! str_replace($dataawal, $dataakhir, $data->data_copywriting_generators) !!}
                        </div>
                </div>
            @elseif (request()->metode == 'buat')
                <form class="col-12" action="#">
                    @csrf
                    <input type="hidden" name="metode" value="tambah">
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Nama Copywriting Generator</label>
                        <div class="col-sm-10">
                            <input name="nama" class="form-control" type="text" value="{{ request()->nama }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Jenis Copywriting</label>
                        <div class="col-sm-4">
                            <select id="jenis" name="jenis" class="form-control" disabled>
                                <option value="{{ request()->jenis }}">{{ request()->jenis }}</option>
                                <option value="">-----</option>
                                @foreach ($jenis as $jns)
                                    <option value="{{ $jns->nama }}">{{ $jns->nama }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-12">
                            <textarea name="data" id="data"></textarea>
                        </div>
                    </div>

                    <a href="{{ route('admin.copywriting.generator') }}" class="btn btn-light">Kembali</a>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </form>

            @else
                <form class="col-12" action="#">
                    <input type="hidden" name="metode" value="buat">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Nama Copywriting Generator</label>
                        <div class="col-sm-9">
                            <input name="nama" class="form-control" type="text">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Jenis Copywriting</label>
                        <div class="col-sm-4">
                            <select id="jenis" name="jenis" class="form-control">
                                @foreach ($jenis as $jns)
                                    <option value="{{ $jns->nama }}">{{ $jns->nama }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Buat</button>
                </form>
        </div><!--card-body-->
    </div><!--card-->
    <div class="card">
        <div class="card-body">
            <div class="col-12">
                <table class="table table-sm">
                    <thead>
                        <th class="text-center">No</th>
                        <th>Nama</th>
                        <th>Data</th>
                        <th>Jenis</th>
                        <th width="150"></th>
                    </thead>
                    <tbody>
                        @foreach ($copywritinggenerators as $key => $generator)
                        <tr>
                            <td class="text-center">{{ $key + $copywritinggenerators->firstItem() }}</td>
                            <td>
                                {{ $generator->nama_copywriting_generators }}
                            </td>
                            <td>
                                {!! $generator->data_copywriting_generators !!}
                            </td>
                            <td>{{ $generator->jenis_copywriting_generators }}</td>
                            <td>
                                <div class="btn-group" role="group" aria-label="Default button group">
                                    <a href="{{ route('admin.copywriting.generator') }}/?metode=edit&id={{ $generator->id }}" class="btn btn-sm btn-success" type="button">Edit</a>
                                    <form action="#" onsubmit="return confirm('Apakah Yakin Dihapus ?');">
                                        @csrf
                                        <input type="hidden" name="metode" value="hapus">
                                        <input name="id" value="{{ $generator->id }}" hidden>
                                        <button class="btn btn-sm btn-danger" >Hapus</button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                        @php
                        $first = $copywritinggenerators->firstItem();
                        $end = $key + $copywritinggenerators->firstItem();
                        @endphp
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="col-12">
                <div class="row" style="padding-top:10px">
                    <div class="col-7">
                        <div class="float-left" style="padding-left:10px">
                            {!! $first !!} - {!! $end !!} Dari {!! $copywritinggenerators->total() !!} Data
                        </div>
                    </div>
                    <!--col-->

                    <div class="col-5">
                        <div class="float-right" style="padding-right:10px">
                            {!! $copywritinggenerators->appends(request()->query())->links() !!}
                        </div>
                    </div>
                    <!--col-->
                </div>
            </div>
            @endif

        </div><!--card-body-->
    </div><!--card-->
</div>
@endsection
