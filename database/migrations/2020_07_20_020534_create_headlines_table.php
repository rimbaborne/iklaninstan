<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHeadlinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('headlines', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('uuid');
            $table->string('keterangan_headlines')->nullable();
            $table->string('contoh_headlines')->nullable();
            $table->string('status_headlines')->default('AKTIF')->nullable();
            $table->string('kategori_headlines')->nullable();
            $table->string('jenis_headlines')->nullable();
            $table->string('sorting_headlines')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('headlines');
    }
}
