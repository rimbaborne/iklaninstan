<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;

/**
 * Class HomeController.
 */
class HomeController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        if (auth()->check()) {
            if (auth()->user()->type === 'admin') {
                return redirect()->route('admin.dashboard');
            }

            if (auth()->user()->type === 'user') {
                return redirect()->route('frontend.user.dashboard');
            }
        }

        // return view('frontend.index');
        return redirect()->route('frontend.auth.login');
        // return redirect('dashboard');
    }
}
